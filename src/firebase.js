import firebase from 'firebase';
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCiPX6yOCp271vdVlBc7KlQm0h905R9994",
    authDomain: "fir-31948.firebaseapp.com",
    databaseURL: "https://fir-31948.firebaseio.com",
    projectId: "fir-31948",
    storageBucket: "fir-31948.appspot.com",
    messagingSenderId: "985625820995",
    appId: "1:985625820995:web:1f185c68349ab190e979f6"
};

// var firebaseConfig = {
//     apiKey: "AIzaSyAXVj6ssgz82HfA4POpeqMERJd7TeGuY4c",
//     authDomain: "food-order-app-ea170.firebaseapp.com",
//     databaseURL: "https://food-order-app-ea170.firebaseio.com",
//     projectId: "food-order-app-ea170",
//     storageBucket: "food-order-app-ea170.appspot.com",
//     messagingSenderId: "84777524874",
//     appId: "1:84777524874:web:aaa4a7ffd959bdd2875773",
//     measurementId: "G-SDL1383QVH"
// };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;