import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { compose } from 'redux';
import Button from '@material-ui/core/Button';
import { Paper, Icon, Select, Switch } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import Hoc from '../../hoc/hoc';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import {
    withStyles,
} from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import SchoolOutlinedIcon from '@material-ui/icons/SchoolOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
import {
    Checkbox,
    Divider,
    FormControlLabel,
    TextField,
    MenuItem
} from '@material-ui/core';
import Image from 'material-ui-image'


const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
        backgroundSize: 'contain'
    },
});

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

class TraineeDetailView extends React.Component {
    state = {
        showPrintDialog: false,
        dataToprint: '',

    }
    render() {
        const handelChangeClick = (e, data) => {
            e.preventDefault();
            if (data == 'mohit') {
                this.setState({ showPrintDialog: true, dataToprint: this.props.rowData.mahoitPatra })
            }
            else if (data == 'nagrikta') {
                this.setState({ showPrintDialog: true, dataToprint: this.props.rowData.nagariktaPatra })
            }
        }
        const handlePrintDialogClose = (e) => {
            this.setState({
                showPrintDialog: false
            })
        }
        const handleEmailDialogClose = (e) => {
            this.setState({
                showEmailDialog: false
            })
        }
        const handleChangeCertificate = (e) => {
            this.setState({
                certificatetype: e.target.value
            })
        }
        const handlePrint = (e) => {
            let w = window.open()
            w.document.write(
                ` <html>
            <head>
            <style>
            .center {
            margin: auto;
            width: 60%;
            padding: 10px;
            }
            </style>
            </head>
            <body>
            <div class="center">
            <img src='${this.state.dataToprint}'/>
            </div>

            </body>
            </html>`
            )
            w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')

            w.document.close() // necessary for IE >= 10
            w.focus() //
            this.setState({
                loading: false,
                showPrintDialog: false,
            })

        }

        const { classes } = this.props;
        return (
            <Hoc>
                <Dialog
                    open={this.state.showPrintDialog}
                    onClose={handlePrintDialogClose}

                    TransitionComponent={Transition}
                    aria-labelledby="draggable-dialog-title"
                >
                    <DialogTitle>प्रमाण-पत्र / प्रशंसा-पत्र</DialogTitle>
                    <DialogContent
                    // style={{ height: "500px", width: '500px' }}
                    >
                        <Image
                            src={this.state.dataToprint}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handlePrint} color="primary" autoFocus>
                            Print
              </Button>
                        <Button onClick={handlePrintDialogClose} color="primary" autoFocus>
                            Cancel
              </Button>
                    </DialogActions>
                </Dialog>
                <div style={{ display: 'flex' }}>

                    <Card className={classes.root} >
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                style={{ height: 140, backgroundSize: 'contain' }}
                                image={this.props.rowData.image}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {this.props.rowData.name}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    पोष्ट: {this.props.rowData.post}<br />
                            उमेर: {this.props.rowData.age}<br />
                            प्रशिक्षण: {this.props.rowData.training_converted}<br />
                            ठेगना: {this.props.rowData.address}<br />
                            जिल्ला: {this.props.rowData.district}<br />
                            वडा नम्बर: {this.props.rowData.ward_number}<br />


                                </Typography>
                            </CardContent>
                        </CardActionArea>

                    </Card>
                    <Card className={classes.root} onClick={(e) => handelChangeClick(e, "mohit")}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={this.props.rowData.mahoitPatra}
                                style={{ height: 140, backgroundSize: 'contain' }}

                                title="Contemplative Reptile"
                            />

                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    मनोनयन पत्र
                        </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    Name: {this.props.rowData.name_english}<br />
                            Post: {this.props.rowData.post_english}<br />
                            Address: {this.props.rowData.address}<br />
                            Institute: {this.props.rowData.trainee_institution_english}<br />

                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                    <Card className={classes.root} onClick={(e) => handelChangeClick(e, "nagrikta")}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                style={{ height: 140, backgroundSize: 'contain' }}
                                image={this.props.rowData.nagariktaPatra}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    नागरिकता
                        </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    प्रशिक्षार्थी संस्था: {this.props.rowData.trainee_institution}<br />
                            समावेशिता: {this.props.rowData.inclusion}<br />
                            लिंग: {this.props.rowData.gender}<br />
                            शैक्षिक योग्यता: {this.props.rowData.educational_qualification}<br />
                            फोन नम्बर: {this.props.rowData.phone_number}<br />
                            इमेल: {this.props.rowData.email}<br />

                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </div>
            </Hoc>

        );
    }
}
export default compose(
    withStyles(useStyles, { withTheme: true }),
)(TraineeDetailView);