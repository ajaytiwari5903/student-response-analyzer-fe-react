import React, { Fragment } from 'react';
import { WrapperSimple } from '../../store/actions/auth';
import MaterialTable from 'material-table';
import { authCheckState } from '../../store/actions/auth';
import axios from 'axios';
import { MTableToolbar } from 'material-table'
import { connect } from "react-redux";
import { Paper, Icon, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Hoc from '../../hoc/hoc';
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Checkbox,
  Divider,
  FormControlLabel,
  TextField,
  MenuItem
} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition } from '../../store/actions/book'
import { compose } from 'redux';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Loading from '../Loading/index';
import { Table } from 'antd';
import 'antd/dist/antd.css';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});





class Component extends React.Component {
  state = {
    data: [],
    formDisplay: false,
    loading: false,
  }
  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .get(window.location.origin + "/api/v1/trainee/")
      .then(res => {
        this.setState({ loading: false })
        let data = res.data
        console.log(data)
        this.setState({
          data: res.data
        })
      }
      )
      .catch(err => {
        this.setState({ loading: false })

        console.log(err)
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


      });

  }
  // static getDerivedStateFromProps(nextProps, prevState,/* nextProps2,prevState2 */) {
  //   if (nextProps.editions && nextProps.editions !== prevState.data) {
  //     return { data: nextProps.editions };
  //   }
  //   else return null;
  // }

  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true })
      console.log(this.state)
    }
    console.log(JSON.parse(localStorage.getItem('categorys')))
    console.log(JSON.parse(localStorage.getItem('categorys')))
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    console.log(this.props.data)


    function onChange(pagination, filters, sorter, extra) {
      console.log('params', pagination, filters, sorter, extra);
    }
    return (
      <Fragment >
        {this.state.loading ?

          <Loading /> :
          (
            <Hoc>
              <Table
                columns={this.props.columns}
                pagination={false}
                dataSource={this.props.data}
                onChange={onChange} />
            </Hoc>

          )
        }
      </Fragment >
    )
  };
}
const mapStateToProps = state => {
  console.log(state.book.categorys)
  return {
    editions: state.book.editions,
    token: state.auth.token,
    loading: state.book.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPostBookEdition: (editionName, token) => dispatch(postBookEdition(editionName, token)),
    onGetBookEdition: (token) => dispatch(getBookEdition(token)),
    ondeleteBookEdition: (editionId, token) => dispatch(deleteBookEdition(editionId, token)),
    editBookEdition: (newEdition, editionId, token) => dispatch(editBookEdition(newEdition, editionId, token)),
    authCheckState: () => dispatch(authCheckState()),

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component);
