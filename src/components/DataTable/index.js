import React from "react";
import MaterialTable from "material-table";


function DataTable() {
const tableRef = React.createRef();

const [totalRow, setTotalRow] = React.useState(0);
const [dataLength, setDataLength] = React.useState(0);




return (
    <MaterialTable
        title="Refresh Data Preview"
        tableRef={tableRef}
        columns={[
            {
                title: 'Avatar',
                field: 'avatar',
                render: rowData => (
                    <img
                        style={{ height: 36, borderRadius: '50%' }}
                        src={rowData.avatar}
                    />
                ),
            },
            { title: 'Id', field: 'id' },
            { title: 'First Name', field: 'first_name' },
            { title: 'Last Name', field: 'last_name' },
        ]}
        data={query =>
            new Promise((resolve, reject) => {
                let url = 'https://reqres.in/api/users?'
                url += 'per_page=' + query.pageSize
                url += '&page=' + (query.page + 1)
                fetch(url)
                    .then(response => response.json())
                    .then(result => {
                        setTotalRow(result.total)
                        setDataLength(result.data.length)
                        resolve({
                            data: result.data,
                            page: result.page - 1,
                            totalCount: result.total,
                        })
                    })
            })
        }
        actions={[
            {
                icon: 'refresh',
                tooltip: 'Refresh Data',
                isFreeAction: true,
                onClick: () => tableRef.current && tableRef.current.onQueryChange(),
            }
        ]}

        onChangeRowsPerPage={(par)=>{
            console.log(par,dataLength,totalRow,tableRef.current,'number')
        }}
    />
)
}
export default DataTable