import React, { Fragment } from 'react';

import ReactApexChart from 'react-apexcharts';

export default class ApexChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '80%',
            // endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: true
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['वैशाख', 'ज्येष्ठ', 'आषाढ़', 'श्रावण', 'भाद्र', 'आश्विन', 'कार्तिक', 'मार्गशीर्ष', 'पौष', 'माघ', 'फाल्गुण', 'चैत्र'],
          // ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
        },
        // yaxis: {
        //   title: {
        //     text: '$ (thousands)'
        //   }
        // },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              // return "$ " + val + " thousands"
              return val
            }
          }
        }
      },


    };
  }


  render() {
    const series = [{
      name: 'प्रशिक्षण',
      data: this.props.trainingCount


    }, {
      name: 'महिला',
      data: this.props.maleCount
    }, {
      name: 'पुरुस',
      data: this.props.femaleCount

    }]
    return (
      <div id="chart">
        <ReactApexChart options={this.state.options} series={series} type="bar" height={350} />
      </div>

    );
  }
}
