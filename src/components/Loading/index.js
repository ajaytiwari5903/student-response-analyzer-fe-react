import React, { Fragment } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from "react-redux";
import {
  withStyles,
} from '@material-ui/core/styles';
import { compose } from 'redux';
import { Grid, Card, CardContent, Button, Divider } from '@material-ui/core';
import { getBookAuthor } from '../../store/actions/book'
import { authCheckState } from '../../store/actions/auth';
import { getSetting } from '../../store/actions/setting';

import Chart from 'react-apexcharts';
const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '44ch'
    },
  },
  DateField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
});
class LivePreviewExample extends React.Component {
  render() {

    return (
      <div class="cube-wrapper" style={{ fontFamily: 'arial' }}>
        <div class="cube-folding">
          <span class="leaf1"></span>
          <span class="leaf2"></span>
          <span class="leaf3"></span>
          <span class="leaf4"></span>
        </div>
        <span class="loading" data-name="Loading">Loading</span>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    token: state.auth.token, 
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth
  };
};
const mapDispatchToProps = dispatch => {
  return {

    ongetBookAuthor: (token) => dispatch(getBookAuthor(token)),
    authCheckState: () => dispatch(authCheckState()),
    getSetting: (token) => dispatch(getSetting(token))
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(LivePreviewExample);
