import React from "react";
import firebase from './../firebase';
import {
    Snackbar,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    TextField,
    Typography,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import { Link } from "react-router-dom";

export default class FirebasePhoneAuthentication extends React.Component {

    state = {
        label: '',
        phoneValidation: '',
        openSnackBar: false,
        openDialog: false,
        shouldShowOTP: false,
        OTPcode: '',
        disabled: false,
        confirmationResult: null,
        firebaseVerificationFailed: false
    }
    componentDidMount() {
        let recaptcha = new firebase.auth.RecaptchaVerifier('recaptcha')
        // recaptcha.render();
        this.setState({
            recaptcha: recaptcha
        })
    }

    handleClick = (event) => {
        if (!this.props.validateData()) {
            return;
        }
        if (this.state.firebaseVerificationFailed) {
            this.props.submit();
            return;
        }
        event.preventDefault()
        this.props.showLoader()
        if (this.props.phoneNumber && this.props.phoneNumber.length === 10) {
            this.state.disabled = true
            let recaptcha = this.state.recaptcha;
            let number = '+977' + this.props.phoneNumber;

            firebase.auth().signInWithPhoneNumber(number, recaptcha).then((e) => {

                this.props.hideLoader()
                this.setState({
                    shouldShowOTP: true,
                    confirmationResult: e,
                    openDialog: true
                })
            })
                .catch(err => {
                    console.log(err)
                    this.props.hideLoader()
                    this.setState({
                        disabled: false,
                        openSnackBar: true,
                        firebaseVerificationFailed: true,
                        label: 'Sorry failed to verify number but saving your information'
                    })
                    this.handleOTPVerification()
                })
        } else {
            this.props.hideLoader()
            this.setState({
                openSnackBar: true,
                label: 'Sorry, mobile number is not valid'
            })
        }
    }

    handleOTPVerification = () => {
        if (this.state.firebaseVerificationFailed) {
            this.props.submit();
            return;
        }
        var code = this.state.OTPcode, e = this.state.confirmationResult
        document.getElementById('recaptcha').innerHTML = '';
        if (code == null) return;
        e.confirm(code).then(result => {
            console.log(result.user, 'user')
            this.setState({
                openSnackBar: true,
                label: "Number verified",
                openDialog: false,
                disabled: false
            })
            this.props.submit();
        })
            .catch(err => {
                console.log(err)
                this.props.submit();
                this.setState({
                    disabled: false
                })

            })
    }

    render() {
        return (
            <div>

                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.openSnackBar} autoHideDuration={6000} onClose={() => {
                        this.setState({
                            openSnackBar: false
                        })

                    }}
                    message={this.state.label}>
                </Snackbar>
                <Dialog open={this.state.openDialog} onClose={() => {
                    this.setState({
                        openDialog: false
                    })
                }}>
                    <DialogTitle>Phone Verification</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            value={this.props.phoneNumber}
                            margin="dense"
                            id="name"
                            label="Phone Number"
                            type="text"
                            fullWidth
                            disabled
                        />
                        {this.state.shouldShowOTP && <TextField
                            autoFocus
                            value={this.state.OTPcode}
                            margin="dense"
                            id="name"
                            label="OTP"
                            type="text"
                            fullWidth
                            onChange={(e) => {
                                this.setState({
                                    OTPcode: e.target.value
                                })
                            }}
                        />}

                    </DialogContent>
                    <DialogActions>
                        {this.state.shouldShowOTP && <Button onClick={this.handleOTPVerification}>
                            Verify
                        </Button>}
                        <Button onClick={() => this.setState({ openDialog: false })}>
                            Cancel
                        </Button>
                    </DialogActions>


                </Dialog>
                <Toolbar>
                    <Button
                        type='submit'
                        variant="outlined"
                        onClick={this.handleClick}
                        disabled={this.state.disabled}
                        style={{ marginRight: '20px' }}
                    >
                        बुझाउनुहोस्</Button>
                    <Link
                        variant="outlined"
                        to={"/"}
                    >
                        <Button
                            variant="outlined"
                        >
                            रद्द गर्नुहोस्</Button>
                    </Link>
                </Toolbar>
            </div>
        )
    }
}