import React from 'react';
import { Redirect } from 'react-router-dom';
import { mainLink } from 'store/utils'
import { updateTrainingInfo, clearTrainingInfo } from 'store/actions/auth';
import Hoc from 'hoc/hoc'
import {
    Button,

    Divider,

    FormControlLabel
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
    withStyles,
} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition, getDetailBookEdition } from 'store/actions/book'
import { compose } from 'redux';
import axios from 'axios'
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import dateMap from '../../pages/Admin/traning/date';
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import FirebasePhoneAuthentication from "../FirebasePhoneAuthentication";
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '100ch'
        },
    },
});

class TraineeRegistrationForm extends React.Component {
    constructor(props) {
        super(props);
        let id = this.props.match.params.id
        console.log(id)
        if (id !== undefined && id !== null) {
            if (this.props.token !== undefined && this.props.token !== null) {
                this.props.ongetDetailBookEdition(this.props.token, id)
            }
        }
    }
    state = {
        name: '',
        post: '',
        name_english: '',
        address: '',
        address_english: '',
        post_english: '',
        age: '',
        training: '',
        trainings: [],
        trainee_institution: '',
        trainee_institution_english: '',
        trainee_institutions: [],
        trainee_institutions_english: [],
        inclusion: '',
        gender: '',
        educational_qualification: '',
        phone_number: '',
        remarks: '',
        redirect: null,
        error: false,
        required: '',
        image: null,
        imageUrl: '',
        district: '',
        ward_number: '',
        email: '',
    }
    componentDidMount() {
        let d = new Date()
        let day = d.getMonth();
        let month = d.getMonth();
        if (day.toString().length == 1) {
            day = '0' + d.getDay()
        } else {
            day = d.getDay()
        }
        if (month.toString().length == 1) {
            month = '0' + d.getMonth()
        } else {
            month = d.getMonth()
        }
        console.log(dateMap[`${d.getFullYear()}-${month}-${day}`])
        this.setState({ dateBS: dateMap[`${d.getFullYear()}-${month}-${day}`] })
        console.log(this.props.match.params.trainingId)
        console.log(this.props.match.params.id)
        // this.props.authCheckState()
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        axios
            .get(window.location.origin + `/api/v1/trainee/trainings/`)
            .then(res => {
                this.setState({
                    trainings: res.data
                })

            })
            .catch(err => {
                this.setState({
                    isErrorDialogPopUp: true,
                    ErrorMessage: 'Failed to load trainings'
                })
            })
        // axios
        //     .get(window.location.origin + `/api/v1/traninigsdetail/${this.props.match.params.trainingId}/`)
        //     .then(res => {
        //             let data = res.data
        //             console.log(data)
        //             let trainingDate = {
        //                 'comittee': res.data.comittee,
        //                 'training': res.data.training
        //             }
        //             this.props.updateTrainingInfo(trainingDate)
        //             // console.log(comittee)
        //             window.localStorage.setItem('trainingDate', JSON.stringify(trainingDate))
        //         }
        //     )
        //     .catch(err => {
        //         console.log(err)
        //         this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
        //
        //
        //     });
        if (this.props.match.params.id != null && this.props.match.params.id != undefined) {
            this.setState({ loading: true })
            axios
                .get(window.location.origin + `/api/v1/trainee/${this.props.match.params.id}/`)
                .then(res => {
                    this.setState({ loading: false })
                    let data = res.data
                    console.log(data)
                    this.setState({
                        age: data.age,
                        educational_qualification: data.educational_qualification,
                        post: data.post,
                        post_english: data.post_english,
                        name: data.name,
                        address: data.address,
                        address_english: data.address_english,
                        name_english: data.name_english,
                        training: this.props.match.params.trainingId,
                        trainee_institution: data.trainee_institution,
                        trainee_institution_english: data.trainee_institution_english,
                        inclusion: data.inclusion,
                        gender: data.gender,
                        phone_number: data.phone_number,
                        remarks: data.remarks,
                        imageUrl: data.image,
                        image: data.image,
                        district: data.district,
                        ward_number: data.ward_number,
                        email: data.email,
                    })
                }
                )
                .catch(err => {
                    this.setState({ loading: false })

                    console.log(err)
                    this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


                });
        }
    }
    componentWillUnmount() {
        this.props.clearTrainingInfo()

    }
    render() {

        const handelSubmit = (e) => {
            e.preventDefault();
            let data = new FormData();
            data.append('dateBS', this.state.dateBS)
            data.append('district', this.state.district);
            data.append('email', this.state.email);
            data.append('ward_number', this.state.ward_number);
            data.append('image', this.state.image);
            data.append('name', this.state.name)
            data.append('name_english', this.state.name_english)
            data.append('address', this.state.address)
            data.append('address_english', this.state.address_english)
            data.append('post', this.state.post)
            data.append('post_english', this.state.post_english)
            data.append('age', this.state.age)
            data.append('training', this.state.tId)
            data.append('trainee_institution', this.state.trainee_institution)
            data.append('trainee_institution_english', this.state.trainee_institution_english)
            data.append('inclusion', this.state.inclusion)
            data.append('gender', this.state.gender)
            data.append('educational_qualification', this.state.educational_qualification)
            data.append('phone_number', this.state.phone_number)
            data.append('remarks', this.state.remarks)
            data.append('mahoitPatra', this.state.mahoitPatra)
            data.append('nagariktaPatra', this.state.nagariktaPatra)

            if (this.state.emailVerification) {
                this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'कृपया मान्य ईमेल ठेगाना दिनुहोस्' })

            } else {
                axios.defaults.headers = {
                    "Content-Type": "application/json",
                    Authorization: `Token ${this.props.token}`,
                };
                axios
                    .post(mainLink + "/api/v1/trainee/", data, {
                        headers: {
                            'content-type': 'multipart/form-data'
                        }
                    })
                    .then(res => {
                        data = res.data
                        console.log(data)
                        if (data.existence) {
                            console.log(data.existence)
                            this.setState({ isErrorDialogPopUp: true, ErrorMessage: data.existence })
                            this.setState({ required: data.required })
                        }
                        else if (data.required) {
                            document.getElementById(data.required).focus();
                        }
                        else if (data.success) {
                            this.setState({ redirect: `/admin/traning-detail/${this.props.match.params.trainingId}` });
                        }

                    }
                    )
                    .catch(err => {
                        console.log(err)
                        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
                    });

            }
            // this.setState({ redirect: '/admin/traning' });


        }
        const handelSubmitAgain = (e) => {
            console.log(this.state)
            e.preventDefault();
            let data = new FormData();
            data.append('dateBS', this.state.dateBS)
            data.append('district', this.state.district);
            data.append('email', this.state.email);
            data.append('ward_number', this.state.ward_number);
            data.append('image', this.state.image);
            data.append('userId', this.props.userId)
            data.append('name', this.state.name)
            data.append('name_english', this.state.name_english)
            data.append('address', this.state.address)
            data.append('address_english', this.state.address_english)
            data.append('post', this.state.post)
            data.append('post_english', this.state.post_english)
            data.append('age', this.state.age)
            data.append('training', this.props.match.params.trainingId)
            data.append('trainee_institution', this.state.trainee_institution)
            data.append('trainee_institution_english', this.state.trainee_institution_english)
            data.append('inclusion', this.state.inclusion)
            data.append('gender', this.state.gender)
            data.append('educational_qualification', this.state.educational_qualification)
            data.append('phone_number', this.state.phone_number)
            data.append('remarks', this.state.remarks)
            if (this.state.emailVerification) {
                this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'कृपया मान्य ईमेल ठेगाना दिनुहोस्' })

            } else {
                axios.defaults.headers = {
                    "Content-Type": "application/json",
                    Authorization: `Token ${this.props.token}`,
                };
                axios
                    .post(mainLink + "/api/v1/trainee/", data)
                    .then(res => {
                        data = res.data
                        console.log(data)
                        if (data.existence) {
                            console.log(data.existence)
                            this.setState({ isErrorDialogPopUp: true, ErrorMessage: data.existence })
                            document.getElementById(data.required).focus();
                        }
                        else if (data.required) {
                            document.getElementById(data.required).focus();
                        }
                        else if (data.success) {
                            this.setState({
                                name: '',
                                post: '',
                                name_english: '',
                                post_english: '',
                                age: '',
                                address_english: '',
                                address: '',
                                training: '',
                                trainings: [],
                                trainee_institution: '',
                                trainee_institution_english: '',
                                trainee_institutions: [],
                                trainee_institutions_english: [],
                                inclusion: '',
                                gender: '',
                                educational_qualification: '',
                                phone_number: '',
                                remarks: '',
                                image: null,
                                imageUrl: '',
                                district: '',
                                ward_number: '',
                                email: '',


                            });
                        }
                    }
                    )
                    .catch(err => {
                        console.log(err)
                        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
                    });
            }
        }
        const handelTraineeInstitutionchange = (event, newInputValue) => {
            this.setState({ trainee_institution: newInputValue })
            let url = '/api/v1/searchtraineeinstitution/?'
            url += 'search_trainee_institution=' + newInputValue
            console.log(newInputValue)
            axios.defaults.headers = {
                "Content-Type": "application/json",
                Authorization: `Token ${this.props.token}`,
            };
            axios
                .get(window.location.origin + url)
                .then(res => {
                    console.log(res.data)
                    let data = res.data
                    if (data.length >= 1) {
                        data.unshift({
                            trainee_institution: 'प्रशिक्षार्थी संस्था',
                            disabled: true
                        })
                        this.state.trainee_institutions = data
                        this.setState({ trainee_institutions: data })
                    }

                    else {
                        this.state.trainee_institutions = []
                        this.setState({ trainee_institutions: [] })
                    }
                }).catch(err => {
                    console.log(err)
                })
        }
        const handelTraineeInstitutionchangeEnglish = (event, newInputValue) => {
            this.setState({ trainee_institution_english: newInputValue })
            newInputValue = newInputValue + '___english'
            let url = '/api/v1/searchtraineeinstitution/?'
            url += 'search_trainee_institution=' + newInputValue
            console.log(newInputValue)
            axios
                .get(window.location.origin + url)
                .then(res => {
                    console.log(res.data)
                    let data = res.data
                    if (data.length >= 1) {
                        data.unshift({
                            trainee_institution_english: 'Institution',
                            disabled: true
                        })
                        this.state.trainee_institutions_english = data
                        this.setState({ trainee_institutions_english: data })
                    }

                    else {
                        this.state.trainee_institutions = []
                        this.setState({ trainee_institutions: [] })
                    }
                }).catch(err => {
                    console.log(err)
                })
        }



        const handelUpdate = () => {
            let data = new FormData();
            data.append('dateBS', this.state.dateBS)
            data.append('district', this.state.district);
            data.append('email', this.state.email);
            data.append('ward_number', this.state.ward_number);
            data.append('image', this.state.image);
            data.append('userId', this.props.userId)
            data.append('name', this.state.name)
            data.append('name_english', this.state.name_english)
            data.append('address', this.state.address)
            data.append('address_english', this.state.address_english)
            data.append('post', this.state.post)
            data.append('post_english', this.state.post_english)
            data.append('age', this.state.age)
            data.append('training', this.state.tId)
            data.append('trainee_institution', this.state.trainee_institution)
            data.append('trainee_institution_english', this.state.trainee_institution_english)
            data.append('inclusion', this.state.inclusion)
            data.append('gender', this.state.gender)
            data.append('educational_qualification', this.state.educational_qualification)
            data.append('phone_number', this.state.phone_number)
            data.append('remarks', this.state.remarks)
            if (this.state.emailVerification) {
                this.setState({ isErrorDialogPopUp: true, ErrorMessage: 'कृपया मान्य ईमेल ठेगाना दिनुहोस्' })

            } else {
                axios.defaults.headers = {
                    "Content-Type": "application/json",
                    Authorization: `Token ${this.props.token}`,
                };
                axios
                    .put(window.location.origin + `/api/v1/trainee/${this.props.match.params.id}/`, data)
                    .then(res => {
                        console.log(res.data)
                        let data = res.data
                        if (data.existence && data.required) {
                            console.log(data.existence)
                            this.setState({ isErrorDialogPopUp: true, ErrorMessage: data.existence })
                            this.setState({ required: data.required })
                        }
                        else if (data.required) {
                            document.getElementById(data.required).focus();
                        }
                        else if (data.success) {
                            this.setState({ redirect: `/admin/traning-detail/${this.props.match.params.trainingId}` });
                        }
                    }).catch(err => {
                        console.log(err)
                    })

            }
        }
        const { classes } = this.props;

        console.log(this.state.detatilEdition, this.state.teptitle)
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }
        const handleErrorClose = () => {
            this.setState({ isErrorDialogPopUp: false })
        };
        const handleImageChange = (e) => {
            if (e.target.files[0]) {
                this.setState({
                    image: e.target.files[0],
                    imageUrl: URL.createObjectURL(e.target.files[0]),
                })
            }
        }
        const handelemailchange = (e) => {
            this.setState({ email: e.target.value })
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)) {
                this.setState({ emailVerification: true })
            }
            else {
                this.setState({ emailVerification: false })
            }
        }

        const handlePatraChange = (e, patra = 'mahoit') => {
            if (e.target.files[0]) {
                this.setState({
                    [patra + 'Patra']: e.target.files[0],
                    [patra + 'PatraUrl']: URL.createObjectURL(e.target.files[0]),
                })
            }
        }

        return (
            <div >
                <Dialog
                    open={this.state.isErrorDialogPopUp}
                    onClose={handleErrorClose}
                    TransitionComponent={Transition}
                    // PaperComponent={PaperComponent}
                    aria-labelledby="draggable-dialog-title"
                >
                    <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">

                        त्रुटि सन्देश
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleErrorClose} color="primary" autoFocus>
                            Ok
                        </Button>
                    </DialogActions>
                </Dialog>
                <Card variant="outlined" >
                    <div style={{ float: 'right', padding: 15 }}>
                        <Link to={`/admin/traning-detail/${this.props.match.params.trainingId}`} ><ArrowBackIcon style={{}} /> </Link>
                    </div>
                    <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>
                        {this.props.match.params.id != null && this.props.match.params.id != undefined ? "प्रशिक्षार्थि सम्पादन गर्नुहोस्" : 'प्रशिक्षार्थि \n' +
                            'दर्ता  गर्नुहोस्'}
                    </div>

                    <Divider className="my-1" />
                    <CardContent style={{ padding: 5 }}>
                        <CardContent className={classes.root} noValidate autoComplete="off">
                            <Button
                                id='image'
                                style={{ height: 200, width: 200, float: 'right' }}
                                component="label"
                            >
                                <input
                                    type="file"
                                    style={{ display: "none", height: 200, width: 200 }}
                                    id="image"
                                    accept="image/png, image/jpeg" onChange={handleImageChange}

                                />
                                <img style={{ height: 200, width: 200 }} src={this.state.imageUrl} />
                            </Button>

                            <FormControlLabel
                                control={
                                    <Autocomplete
                                        id="trainee_training"
                                        // disableClearable
                                        freeSolo

                                        autoHighlight
                                        autoComplete
                                        options={this.state.trainings}
                                        inputValue={this.state.training}
                                        getOptionDisabled={(option) => option.disabled}
                                        getOptionLabel={(option) => option.name}
                                        style={{ width: "50%" }}
                                        onInputChange={(event, newInputValue) => {
                                            var data = this.state.trainings.filter(item => item.name === newInputValue)
                                            if (data.length) {
                                                this.setState({
                                                    training: newInputValue,
                                                    tId: data[0].id
                                                })
                                            } else {
                                                this.setState({
                                                    training: newInputValue
                                                })
                                            }
                                        }
                                        }
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                id="trainee_training"
                                                label="प्रशिक्षण "
                                                type='text' name='trainee_training'
                                                style={{ width: "100%" }}
                                            // InputProps={{ ...params.InputProps, type: 'search' }}
                                            />
                                        )}
                                    />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="name"
                                        autoFocus

                                        label="
                    नाम" type='text' name='name'
                                        defaultValue={this.state.name}
                                        value={this.state.name}
                                        onChange={(e) => { this.setState({ name: e.target.value }) }}
                                        style={{ width: "50%" }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="name_english"
                                        defaultValue={this.state.name_english}
                                        label="
                    Name" type='text' name='name_english'
                                        value={this.state.name_english}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ name_english: e.target.value }) }} />
                                }
                            />

                            <FormControlLabel
                                control={
                                    <TextField id="post"
                                        defaultValue={this.state.post}
                                        label="पद" type='text' name='post'
                                        value={this.state.post}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ post: e.target.value }) }} />
                                }
                            />

                            <FormControlLabel
                                control={
                                    <TextField id="post_english"
                                        defaultValue={this.state.post_english}
                                        label="Post" type='text' name='post_english'
                                        value={this.state.post_english}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ post_english: e.target.value }) }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="address"
                                        defaultValue={this.state.address}
                                        onChange={(e) => { this.setState({ address: e.target.value }) }}
                                        label="
                    ठेगाना" type='text' name='address'
                                        value={this.state.address}
                                        style={{ width: "50%" }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="address_english"
                                        defaultValue={this.state.address_english}
                                        label="Address" type='text' name='address_english'
                                        value={this.state.address_english}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ address_english: e.target.value }) }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="age"
                                        defaultValue={this.state.age}
                                        label="
                    उमेर" type='text' name='age'
                                        value={this.state.age}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ age: e.target.value.replace(/[\D\s\._\-]+/g, "") }) }} />
                                }
                            />
                            {/* <FormControlLabel
                control={
                  <TextField id="training"
                    defaultValue={this.state.training}
                    label="
                    प्रशिक्षण" type='text' name='training'
                    value={this.state.training}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ training: e.target.value }) }} />
                }
              /> */}
                            <FormControlLabel
                                control={
                                    <Autocomplete
                                        freeSolo
                                        id="trainee_institution"
                                        // disableClearable
                                        label="प्रशिक्षार्थी संस्था"
                                        autoHighlight

                                        options={this.state.trainee_institutions}
                                        inputValue={this.state.trainee_institution}
                                        getOptionDisabled={(option) => option.disabled}
                                        getOptionLabel={(option) => option.trainee_institution}
                                        style={{ width: "50%" }}
                                        onInputChange={(event, newInputValue) => handelTraineeInstitutionchange(event, newInputValue)}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                id="trainee_institution"
                                                label="प्रशिक्षार्थी संस्था" type='text' name='trainee_institution'
                                                style={{ width: "100%" }}
                                            // InputProps={{ ...params.InputProps, type: 'search' }}
                                            />
                                        )}
                                    />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <Autocomplete
                                        id="trainee_institution_english"
                                        // disableClearable
                                        freeSolo
                                        label="प्रशिक्षार्थी संस्था"
                                        autoHighlight
                                        autoComplete
                                        options={this.state.trainee_institutions_english}
                                        inputValue={this.state.trainee_institution_english}
                                        getOptionDisabled={(option) => option.disabled}
                                        getOptionLabel={(option) => option.trainee_institution_english}
                                        style={{ width: "50%" }}
                                        onInputChange={(event, newInputValue) => handelTraineeInstitutionchangeEnglish(event, newInputValue)}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                id="trainee_institution_english"
                                                label="Institution" type='text' name='trainee_institution_english'
                                                style={{ width: "100%" }}
                                            // InputProps={{ ...params.InputProps, type: 'search' }}
                                            />
                                        )}
                                    />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="district"
                                        defaultValue={this.state.district}
                                        label="जिल्ला" type='text' name='district'
                                        value={this.state.district}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ district: e.target.value }) }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="ward_number"
                                        defaultValue={this.state.ward_number}
                                        label="वार्ड नम्बर" type='text' name='ward_number'
                                        value={this.state.ward_number}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ ward_number: e.target.value }) }} />
                                }
                            />
                            <FormControlLabel
                                lable='ईमेल'
                                defaultValue={this.state.email}

                                control={
                                    <TextField label="ईमेल" type='email' name='email'
                                        id="email"
                                        onChange={handelemailchange}
                                        lable='ईमेल'
                                        value={this.state.email}

                                        style={{ width: '50%' }}
                                        helperText={this.state.emailVerification ? <a style={{ color: "red" }} >अवैध ईमेल</a> : ""}
                                    />
                                }
                            />


                            <FormControlLabel
                                control={
                                    <Autocomplete

                                        id="inclusion"
                                        // disableClearable
                                        label="समावेशिता"
                                        options={['समावेशिता', 'दलित', 'आदिवासी/जनजाति', 'मधेसी', 'अपाङ्ग', 'अन्य']}
                                        inputValue={this.state.inclusion}
                                        autoHighlight
                                        autoComplete
                                        autoSelect
                                        getOptionDisabled={(option) => option == 'समावेशिता'}
                                        getOptionLabel={(option) => option}
                                        style={{ width: "50%" }}
                                        onInputChange={(event, newInputValue) => this.setState({ inclusion: newInputValue })}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                id="inclusion"
                                                label="समावेशिता" type='text' name='inclusion'
                                                style={{ width: "100%" }}
                                                InputProps={{ ...params.InputProps, type: 'search' }}
                                                onKeyDownCapture={(e) => {
                                                    if (e.keyCode == 13) {
                                                        document.getElementById('gender').focus();
                                                    }
                                                }}
                                            />
                                        )}
                                    />
                                }
                            />


                            <FormControlLabel
                                control={
                                    <Autocomplete

                                        id="gender"
                                        // disableClearable
                                        label="लिंग"
                                        options={['लिंग', 'पुरुष', 'महिला', 'अन्य']}
                                        inputValue={this.state.gender}
                                        autoHighlight
                                        autoComplete
                                        autoSelect
                                        getOptionDisabled={(option) => option == 'लिंग'}
                                        getOptionLabel={(option) => option}
                                        style={{ width: "50%" }}
                                        onInputChange={(event, newInputValue) => this.setState({ gender: newInputValue })}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                id="gender"
                                                defaultValue={this.state.gender}
                                                label="लिंग" type='text' name='gender'
                                                value={this.state.gender}
                                                style={{ width: "100%" }}
                                                InputProps={{ ...params.InputProps, type: 'search' }}
                                                onKeyDownCapture={(e) => {
                                                    if (e.keyCode == 13) {
                                                        document.getElementById('educational_qualification').focus();
                                                    }
                                                }}
                                            />
                                        )}
                                    />
                                }
                            />


                            <FormControlLabel
                                control={
                                    <TextField id="educational_qualification"
                                        defaultValue={this.state.educational_qualification}
                                        label="
                    शैक्षिक योग्यता" type='text' name='educational_qualification'
                                        value={this.state.educational_qualification}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ educational_qualification: e.target.value }) }} />
                                }
                            /><FormControlLabel
                                control={
                                    <TextField id="phone_number"
                                        defaultValue={this.state.phone_number}
                                        onChange={(e) => { this.setState({ phone_number: e.target.value.replace(/[\D\s\._\-]+/g, "") }) }}
                                        label="
                    सम्पर्क नम्बर" type='text' name='phone_number'
                                        value={this.state.phone_number}
                                        style={{ width: "50%" }} />
                                }
                            />
                            <FormControlLabel
                                control={
                                    <TextField id="remarks"
                                        defaultValue={this.state.remarks}
                                        label="कैक्रफयत" type='text' name='remarks'
                                        value={this.state.remarks}
                                        style={{ width: "50%" }} onChange={(e) => { this.setState({ remarks: e.target.value }) }} />
                                }
                            />


                            <FormControlLabel control={
                                <React.Fragment>
                                    मोहित पत्र
                                    <input accept="image/png" style={{ width: '200px', display: 'none' }} onChange={(e) => handlePatraChange(e)} className={classes.input} id="sig-button-file" type="file" />
                                    <label htmlFor="sig-button-file">
                                        <IconButton color="primary" aria-label="upload picture" component="span">
                                            {this.state.mahoitPatraUrl ?
                                                <img style={{ height: 100, width: 100, float: 'right' }} src={this.state.mahoitPatraUrl} /> :
                                                <PhotoCamera />
                                            }
                                        </IconButton>
                                    </label>
                                </React.Fragment>
                            } />
                            <FormControlLabel control={
                                <>
                                    नागरिकता
                                    <input accept="image/png" style={{ width: '200px', display: 'none' }} onChange={(e) => handlePatraChange(e, 'nagarikta')} className={classes.input} id="imgsig-button-file" type="file" />
                                    <label htmlFor="imgsig-button-file">
                                        <IconButton color="primary" aria-label="upload picture" component="span">
                                            {this.state.nagariktaPatraUrl ?
                                                <img style={{ height: 100, width: 100, float: 'right' }} src={this.state.nagariktaPatraUrl} /> :
                                                <PhotoCamera />
                                            }
                                        </IconButton>
                                    </label>
                                </>
                            } />


                            {this.props.match.params.id != null && this.props.match.params.id != undefined ?
                                <div style={{ width: '500px' }}>

                                    <Button
                                        type='submit'
                                        variant="outlined"
                                        onClick={handelUpdate}
                                    >
                                        बुझाउनुहोस्</Button>
                                </div>
                                :
                                <div style={{ width: '500px' }}>
                                    <Hoc>
                                        <div id="recaptcha" />
                                        <FirebasePhoneAuthentication submit={handelSubmit} phoneNumber={this.state.phone_number} />


                                    </Hoc>
                                </div>

                            }

                        </CardContent>
                    </CardContent>
                </Card>
            </div >
        )
    }
}
const mapStateToProps = state => {
    console.log(state.book.detatilEdition)
    return {
        token: null,
        loading: state.book.loading,
        error: state.book.error,
        isAuthenticated: state.auth.token !== null,
        successAuth: state.auth.successAuth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateTrainingInfo: (data) => dispatch(updateTrainingInfo(data)),
        clearTrainingInfo: () => dispatch(clearTrainingInfo()),
        onPostBookEdition: (editionName, token) => dispatch(postBookEdition(editionName, token)),
        onGetBookEdition: (token) => dispatch(getBookEdition(token)),
        ondeleteBookEdition: (editionId, token) => dispatch(deleteBookEdition(editionId, token)),
        oneditBookEdition: (newEdition, editionId, token) => dispatch(editBookEdition(newEdition, editionId, token)),
        ongetDetailBookEdition: (token, id) => dispatch(getDetailBookEdition(token, id)),
    };
};
export default compose(
    withStyles(useStyles, { withTheme: true }),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(TraineeRegistrationForm);
