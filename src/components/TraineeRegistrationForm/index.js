import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField'
import { BootstrapInput } from './Review';
import logo from "../../assets/Image_1.png"
import { Box, Grid, Typography } from "@material-ui/core";
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from "axios";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import FirebasePhoneAuthentication from "../FirebasePhoneAuthentication";
import Hoc from "../../hoc/hoc";
import { mainLink } from "../../store/utils";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import CircularProgress from "@material-ui/core/CircularProgress";
import addressData from "./addressData";
import { Redirect } from "react-router";
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link target="_blank" color="inherit" href="http://ctrc.gov.np/">
                सहकारी प्रशिक्षण तथा अनुसन्धान केन्द्र
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = (theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginTop: theme.spacing(4),
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(1000 + theme.spacing(2) * 2)]: {
            width: '80%',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(1000 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
});

class TraineeRegistrationForm extends React.Component {

    state = {
        prevKeyCode: 1,
        trainee_training: '',
        first_name: '',
        last_name: '',
        middle_name: '',
        first_name_english: "",
        last_name_english: "",
        middle_name_english: "",
        name: '',
        post: '',
        name_english: '',
        address: '',
        address_english: '',
        post_english: '',
        age: '',
        training: '',
        trainings: [],
        trainee_institution: '',
        trainee_institution_english: '',
        trainee_institutions: [],
        trainee_institutions_english: [],
        inclusion: '',
        gender: '',
        educational_qualification: '',
        phone_number: '',
        remarks: '',
        redirect: null,
        error: false,
        required: '',
        image: null,
        imageUrl: '',
        nagariktaPatraUrl: '',
        mahoitPatraUrl: '',
        district: '',
        ward_number: '',
        loading: false,
        email: '',
        province: '',
        provinceIndex: 0,
        citizenship_issued_district: '',
        citizenship_number: '',
        districtIndex: 'Bhojpur',
        provinces: addressData.provinces.np,
        districts: addressData.districts.np[0],
        municipalities: addressData.palikas.np[0]['झापा'],
        municipality: '',
        showSnackBar: false,
        snackbarMessage: '',
        dartaDialog: false,
        isErrorDialogPopUp: false,
    }


    componentDidMount() {
        // const script = document.createElement("script");
        // script.src = "/static/unicode.js";
        // script.async = true;
        // document.body.appendChild(script);
        // let list = []
        // console.log(addressData.palikas.np)
        // for (let i = 0; i < addressData.palikas.np.length; i++) {
        //     let keys = Object.keys(addressData.palikas.np[i])
        //     console.log(keys)
        //     // for (let j = 0; i < keys.length; j++) {
        //     //     console.log(addressData.palikas.np[i][keys[j]])

        //     // }

        //     // console.log(addressData.palikas.np[i])

        // }
        axios.defaults.headers = {
            "Content-Type": "application/json",
        };
        axios
            .get(window.location.origin + `/api/v1/trainee/trainings/?self`)
            .then(res => {
                if (typeof res.data === "object" && res.data.length) {
                    this.setState({
                        trainings: res.data
                    })
                } else {
                    this.setState({
                        redirect: `/`
                    })
                }


            })
            .catch(err => {
                this.setState({
                    isErrorDialogPopUp: true,
                    ErrorMessage: 'Failed to load trainings'
                })
            })
    }

    checkImageType = (file, message) => {
        if (!file.type.match('image/')) {
            this.setState({
                showSnackBar: true,
                snackbarMessage: message
            })
        }
        return file.type.match('image/')
    }

    handleImageChange = (e) => {
        if (e.target.files[0] && this.checkImageType(e.target.files[0], 'कृपया फोटो हल्नुहोस्')) {

            this.setState({
                image: e.target.files[0],
                imageUrl: URL.createObjectURL(e.target.files[0]),
            })
        }
    }

    triggerLoad = () => {
        this.setState({
            loading: true
        })
    }

    triggerStopLoad = () => {
        this.setState({
            loading: false
        })
    }

    handleRadioCheck = (e) => {
        if (e.target.checked) {
            this.setState({
                [e.target.name]: e.target.value
            })
        }

    }

    handleTextChange = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        })


    }



    handelemailchange = (e) => {
        this.setState({ email: e.target.value })
        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)) {
            this.setState({ emailVerification: true })
        }
        else {
            this.setState({ emailVerification: false })
        }
    }

    handleErrorClose = () => {
        this.setState({ isErrorDialogPopUp: false })
    };


    handlePatraChange = (e, patra = 'mahoit') => {
        if (e.target.files[0] && this.checkImageType(e.target.files[0], 'File should be image')) {
            let file = e.target.files[0]
            this.setState({
                [patra + 'Patra']: e.target.files[0],
                [patra + 'PatraType']: file.type,
                [patra + 'PatraName']: file.name,
                [patra + 'PatraUrl']: URL.createObjectURL(e.target.files[0]),
            })
        }
    }

    handelSubmit = () => {
        let data = new FormData();
        data.append('dateBS', this.state.dateBS)
        data.append('district', this.state.district);
        data.append('citizenship_issued_district', this.state.citizenship_issued_district);
        data.append('citizenship_number', this.state.citizenship_number);
        data.append('email', this.state.email);
        data.append('ward_number', this.state.ward_number);
        data.append('image', this.state.image);
        data.append('name', this.state.first_name + (this.state.middle_name === '' ? '' : ' ' + this.state.middle_name) + ' ' + this.state.last_name)
        data.append('name_english', this.state.first_name_english + (this.state.middle_name_english === '' ? '' : ' ' + this.state.middle_name_english) + ' ' + this.state.last_name_english)
        data.append('address', this.state.address)
        data.append('address_english', 'Required')
        data.append('post', this.state.post)
        data.append('post_english', this.state.post_english)
        data.append('age', this.state.age)
        data.append('training', this.state.tId)
        data.append('trainee_institution', this.state.trainee_institution)
        data.append('trainee_institution_english', this.state.trainee_institution_english)
        data.append('inclusion', this.state.inclusion)
        data.append('gender', this.state.gender)
        data.append('educational_qualification', this.state.educational_qualification)
        data.append('phone_number', this.state.phone_number)
        data.append('remarks', this.state.remarks)
        data.append('mahoitPatra', this.state.mahoitPatra)
        data.append('nagariktaPatra', this.state.nagariktaPatra)
        data.append('province', this.state.province)
        data.append('municipality', this.state.municipality)

        if (this.state.emailVerification) {
            this.setState({ isErrorDialogPopUp: true, loading: false, ErrorMessage: 'कृपया मान्य ईमेल ठेगाना दिनुहोस्' })

        } else {
            axios.defaults.headers = {
                "Content-Type": "application/json",
                Authorization: `Token ${this.props.token}`,
            };
            axios
                .post(mainLink + "/api/v1/trainee/", data, {
                    headers: {
                        'content-type': 'multipart/form-data'
                    }
                })
                .then(res => {
                    data = res.data

                    if (data.success) {
                        this.setState({ loading: false, dartaDialog: true });
                    } else if (data.existence) {

                        this.setState({ isErrorDialogPopUp: true, loading: false, ErrorMessage: data.existence })
                        this.setState({ required: data.required })
                    }
                    else if (data.required) {
                        document.getElementById(data.required).focus();
                        this.triggerStopLoad()
                    }


                }
                )
                .catch(err => {
                    console.log(err)
                    this.setState({ isErrorDialogPopUp: true, loading: false, ErrorMessage: err.message })
                });

        }
        // this.setState({ redirect: '/admin/traning' });


    }

    getDistricts = (value) => {
        let index = addressData.provinces.np.indexOf(value)
        if (index >= 0 && index < addressData.provinces.np.length) {
            return addressData.districts.np[index];
        }
        return this.state.districts;
    }

    getMunicipalities = (district) => {
        let index = this.state.districts.indexOf(district)
        if (index >= 0 && index < this.state.districts.length) {
            let districtIndex = addressData.districts.en[this.state.provinceIndex][index];
            if (districtIndex in addressData.palikas.np[this.state.provinceIndex]) {
                return addressData.palikas.np[this.state.provinceIndex][districtIndex]
            }
            return this.state.municipalities;
        }
        return this.state.municipalities;
    }

    validateData = () => {
        console.log('imageUrl', this.state.imageUrl, this.state.imageUrl === '')


        var dataForValidation = [
            'trainee_training',
            'first_name',
            'last_name',
            'first_name_english',
            'last_name_english',
            'gender',
            'age',
            'address',
            'phone_number',
            'email',
            'post_english',
            'post',
            'educational_qualification',
            'inclusion',
            'citizenship_number',
            'citizenship_issued_district',
            'trainee_institution',
            'trainee_institution_english',
            'province',
            'district',
            'municipality',
            'ward_number',
        ]
        var valid = true
        for (let data in dataForValidation) {
            if (!valid) {
                return valid
            }


            valid = (this.state[dataForValidation[data]] !== '')
            if (dataForValidation[data] === 'gender' && this.state.gender === '') {
                this.setState({
                    showSnackBar: true,
                    snackbarMessage: 'कृपया लिङ्ग छान्नुहोस्'
                })
                return false
            }
            if (!valid) {
                // console.log('refs', this[this.dataForValidation[data]+'Ref'].current.querySelector('div').querySelector('input').focus())
                if (document.getElementById(dataForValidation[data])) {
                    document.getElementById(dataForValidation[data]).focus()
                }


            }

            if (valid) {
                switch (dataForValidation[data]) {
                    case 'email':
                        if (typeof this.state.email === 'string' && this.state.emailVerification) {

                            this.setState({
                                showSnackBar: true,
                                snackbarMessage: 'कृपया ईमेल सच्यौनुहोस्'
                            })
                            if (document.getElementById(dataForValidation[data])) {
                                document.getElementById(dataForValidation[data]).focus()
                            }
                            return false;
                        }
                        break
                    case 'age':
                        if (typeof this.state.age && this.state.age.toString().length > 2 && parseInt(this.state.age) < 5) {

                            this.setState({
                                showSnackBar: true,
                                snackbarMessage: 'कृपया उमेर सच्यौनुहोस्'
                            })
                            if (document.getElementById(dataForValidation[data])) {
                                document.getElementById(dataForValidation[data]).focus()
                            }
                            return false;
                        }
                        break
                    case 'phone_number':
                        if (typeof this.state.phone_number && this.state.phone_number.toString().length !== 10) {

                            this.setState({
                                showSnackBar: true,
                                snackbarMessage: 'कृपया सम्पर्क नम्बर सच्यौनुहोस्'
                            })
                            if (document.getElementById(dataForValidation[data])) {
                                document.getElementById(dataForValidation[data]).focus()
                            }
                            return false;
                        }
                        break
                    default:
                        break
                }
            }
        }
        if (this.state.imageUrl === '') {
            this.setState({
                showSnackBar: true,
                snackbarMessage: 'कृपया फोटो हल्नुहोस्'
            })
            return false
        }
        if (this.state.nagariktaPatraUrl === '') {
            this.setState({
                showSnackBar: true,
                snackbarMessage: 'कृपया नागरिकता हल्नुहोस्'
            })
            return false
        }
        if (this.state.mahoitPatraUrl === '') {
            this.setState({
                showSnackBar: true,
                snackbarMessage: 'कृपया मनोनयन पत्र हल्नुहोस्'
            })
            return false
        }
        return valid

    }

    moveCursor(event, prev, next) {

        if (event.keyCode) {
            if (event.key === 'Tab') {
                event.preventDefault()
                document.getElementById(next).focus()
            }
            // if(event.keyCode===16&&document.getElementById(prev)){
            //     event.preventDefault();
            //     document.getElementById(prev).focus()
            // }
            // if(!isNaN(this.state.prevKeyCode)&&this.state.prevKeyCode!==16&&event.keyCode===9&&document.getElementById(next)){
            //     event.preventDefault();
            //     document.getElementById(next).focus()
            // }
            // this.state.prevKeyCode = event.keyCode
        }
    }

    handleSnackClose = () => {
        this.setState({
            showSnackBar: false,
            snackbarMessage: ''

        })
    }

    handleDartaDialogClose = () => {
        this.setState({
            dartaDialog: false,
            redirect: `/`

        })
    }

    render() {
        function MouseOver(event) {
            event.target.style.background = '#eeeeee';
        }
        function MouseOut(event) {
            event.target.style.background = 'transparent';
        }
        return (
            this.state.redirect ? <Redirect to={this.state.redirect} /> :
                <React.Fragment>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={this.state.showSnackBar}
                        autoHideDuration={2000}
                        onClose={this.handleSnackClose}
                        message={this.state.snackbarMessage}
                        action={
                            <React.Fragment>
                                <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleSnackClose}>
                                    <CloseIcon fontSize="small" />
                                </IconButton>
                            </React.Fragment>
                        }
                    />
                    <Dialog
                        open={this.state.isErrorDialogPopUp}
                        onClose={this.handleErrorClose}
                        TransitionComponent={Transition}
                        // PaperComponent={PaperComponent}
                        aria-labelledby="draggable-dialog-title"
                    >
                        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">

                            त्रुटि सन्देश
                    </DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                <a href="#" severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleErrorClose} color="primary" autoFocus>
                                Ok
                        </Button>
                        </DialogActions>
                    </Dialog>
                    <Dialog
                        open={this.state.dartaDialog}
                        onClose={this.handleDartaDialogClose}
                        TransitionComponent={Transition}
                        // PaperComponent={PaperComponent}
                        aria-labelledby="draggable-dialog-title"
                    >
                        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                            दर्ताको लागि धन्यवाद, तपाईंको फारम चाँडै नै स्वीकृत हुनेछ।
                    </DialogTitle>
                        <DialogActions>
                            <Button onClick={this.handleDartaDialogClose} color="primary" autoFocus>
                                ठिक छ
                        </Button>
                        </DialogActions>
                    </Dialog>

                    <CssBaseline />
                    <AppBar position="absolute" style={{ backgroundColor: 'rgba(0,59,101,1)', height: '120px' }} className={this.props.classes.appBar}>
                        <Toolbar>
                            <Grid
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}
                            >
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="h6" color="inherit" noWrap>
                                            <img src={logo} alt="logo" style={{ width: '100px', marginTop: "16px" }} />
                                        </Typography>
                                        <Typography variant="h4" color="inherit" style={{ marginTop: '16px', marginLeft: '10px' }} noWrap>
                                            <span style={{ fontWeight: 'bold' }}>सहकारी प्रशिक्षण तथा अनुसन्धान केन्द्र </span><br />
                                        Cooperative Training and Research Centre <br />
                                            <span style={{ fontSize: '15px' }}>नयाँ वानेश्वर, काठमाडौं</span>
                                        </Typography>
                                    </Toolbar>

                                </Grid>

                                <Grid item>
                                    <Typography variant="h6" color="inherit" noWrap>
                                        <img src="https://www.animatedimages.org/data/media/839/animated-nepal-flag-image-0007.gif" alt="logo" style={{ width: '90px', marginTop: '4px' }} />
                                    </Typography>
                                </Grid>
                            </Grid>


                        </Toolbar>
                    </AppBar>
                    <main className={this.props.classes.layout}>
                        <Typography variant="h4" style={{ color: 'rgba(0,59,101,1)' }} noWrap>
                            Trainee Registration (प्रशिक्षार्थि दर्ता गर्नुहोस्)
                    </Typography>
                        <div style={{ float: 'right', padding: 15 }}>
                            <Link href="/"><ArrowBackIcon style={{ borderRadius: '50%', width: '40px', height: '40px', padding: 8 }} onMouseOver={MouseOver} onMouseOut={MouseOut} /> </Link>
                        </div>
                        {/*Training Information*/}
                        <Box width="100%" style={{ marginTop: '60px', paddingBottom: '10px', border: '1.5px solid rgb(112 112 112 / 55%)' }}>
                            <Typography variant="h5" style={{
                                position: "relative",
                                top: "-11px",
                                background: "#fafafa",
                                width: "max-content",
                                left: "20px",
                                paddingLeft: "5px",
                                paddingRight: " 5px"
                            }} noWrap>
                                Training Information(प्रशिक्षण विवरण)
                        </Typography>
                            <Grid style={{ fontSize: '16px', marginRight: '120px' }}>
                                <Toolbar>
                                    <Typography variant="body1" style={{ marginRight: '25px' }}>
                                        Training Name:
                                </Typography>
                                    <Autocomplete
                                        id="trainee_training combo-box-demo"
                                        style={{ width: '100%' }}

                                        autoComplete
                                        options={this.state.trainings}
                                        inputValue={this.state.training}

                                        getOptionDisabled={(option) => option.disabled}
                                        getOptionLabel={(option) => option.name}
                                        onInputChange={(event, newInputValue) => {
                                            var data = this.state.trainings.filter(item => item.name === newInputValue)
                                            if (data.length) {
                                                this.setState({
                                                    training: newInputValue,
                                                    trainee_training: newInputValue,
                                                    tId: data[0].id
                                                })
                                            } else {
                                                this.setState({
                                                    training: newInputValue
                                                })
                                            }
                                        }
                                        }
                                        renderInput={(params) => (
                                            <div ref={params.InputProps.ref}>
                                                <TextField placeholder="प्रशिक्षण छान्नुहोस्" {...params} {...params.inputProps} variant="outlined" fullWidth />
                                            </div>
                                        )}
                                    />

                                </Toolbar>
                            </Grid>
                        </Box>
                        {/*Personal Information*/}
                        <Box width="100%" style={{ marginTop: '60px', border: '1.5px solid rgb(112 112 112 / 55%)' }}>
                            <Typography variant="h5" style={{
                                position: "relative",
                                top: "-11px",
                                background: "#fafafa",
                                width: "max-content",
                                left: "20px",
                                paddingLeft: "5px",
                                paddingRight: " 5px"
                            }} noWrap>
                                Personal Information(व्यक्तिगत विवरण)
                        </Typography>
                            <Grid
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}
                            >
                                <Grid item>
                                    <Typography variant="body1" color="inherit" noWrap >
                                        <br />
                                    &ensp;&ensp;&ensp;नाम(देवनागरी):
                                    <br />
                                        <br />
                                        <br />
                                    &ensp;&ensp;&ensp;Name(अङ्रेजिमा):
                                </Typography>

                                </Grid>
                                <Grid item style={{marginLeft:'-51px'}}>
                                    <Typography variant="body1" color="inherit" noWrap>
                                        पहिलो(First)<br />
                                        <BootstrapInput type="text"
                                            id="first_name"


                                            className="UnicodeNepali"
                                            value={this.state.first_name}
                                            onChange={(e) => { this.setState({ first_name: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'trainee_training', 'middle_name')}
                                        />

                                        <br />
                                        <br />
                                        <BootstrapInput
                                            type="text"
                                            id="first_name_english"
                                            value={this.state.first_name_english}
                                            onChange={(e) => { this.setState({ first_name_english: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'last_name', 'middle_name_english')}
                                        />

                                    </Typography>

                                </Grid>
                                <Grid item style={{marginLeft:'-51px'}}>
                                    <Typography variant="body1" color="inherit" noWrap>
                                        बीचको(Middle)<br />
                                        <BootstrapInput type="text"
                                            id="middle_name"
                                            value={this.state.middle_name}
                                            onChange={(e) => { this.setState({ middle_name: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'first_name', 'last_name')}
                                        />

                                        <br />
                                        <br />
                                        <BootstrapInput type="text"
                                            id="middle_name_english"
                                            value={this.state.middle_name_english}
                                            onChange={(e) => { this.setState({ middle_name_english: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'first_name_english', 'last_name_english')}
                                        />

                                    </Typography>

                                </Grid>
                                <Grid item style={{marginLeft:'-51px'}} >
                                    <Typography variant="body1" color="inherit" noWrap>
                                        थर(Last)<br />
                                        <BootstrapInput type="text"
                                            id="last_name"
                                            value={this.state.last_name}
                                            onChange={(e) => { this.setState({ last_name: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'middle_name', 'first_name_english')}
                                        />

                                        <br />
                                        <br />
                                        <BootstrapInput type="text"
                                            id="last_name_english"
                                            value={this.state.last_name_english}
                                            onChange={(e) => { this.setState({ last_name_english: e.target.value.replace(' ', '').replace(/[\d]+$/, '') }) }}
                                            onKeyDown={(e) => this.moveCursor(e, 'middle_name_english', 'gender_male')}
                                        />

                                    </Typography>

                                </Grid>

                                <Grid item>
                                    <Typography variant="h6" color="inherit" noWrap>
                                        <Button
                                            id='image'
                                            style={{ width: 150, height: 150, float: 'right', marginRight: '20px', backgroundColor: 'rgba(0, 0, 0, 0.04)' }}
                                            component="label"
                                        >

                                            <input
                                                type="file"
                                                style={{ display: "none" }}
                                                id="image"
                                                accept="image/png, image/jpeg" onChange={this.handleImageChange}

                                            />
                                            <img alt="याहा फोटो हल्नुहोस्" style={{ width: 150, height: 150 }} src={this.state.imageUrl} />
                                        </Button>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="subtitle1">
                                            लिङ्ग:
                                    </Typography>
                                        <Toolbar style={{ marginLeft: '10%', fontSize: '16px' }}>
                                            <input id="gender_male" name="gender" value="पुरुष" onClick={this.handleRadioCheck} type="radio" style={{ width: '50px' }} />
                                        पुरुष
                                    </Toolbar>
                                        <Toolbar style={{ fontSize: '16px' }}>
                                            <input name="gender" value="महिला" onClick={this.handleRadioCheck} type="radio" style={{ width: '50px' }} />
                                        महिला
                                    </Toolbar>
                                        <Toolbar style={{ fontSize: '16px' }}>
                                            <input value="अन्य" name="gender" onClick={this.handleRadioCheck} type="radio" style={{ width: '50px' }} />
                                        अन्य
                                    </Toolbar>

                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar style={{ fontSize: '16px' }}>
                                        <Typography variant="body1" style={{ marginRight: '86px' }}>
                                            Age:
                                    </Typography>

                                        <Typography variant="body1" component="div" color="inherit" noWrap >
                                            <BootstrapInput
                                                style={{ width: 260 }}
                                                placeholder="उमेर अङ्रेजीमा"
                                                type="text"
                                                onChange={(e) => { this.setState({ age: e.target.value.replace(/[\D\s\._\-]+/g, "") }) }}
                                                id="age"
                                                value={this.state.age}
                                            />
                                        </Typography>

                                    </Toolbar>
                                </Grid>
                            </Grid>
                            <Grid style={{ fontSize: '16px', marginRight: '120px' }}>
                                <Toolbar>
                                    <Typography variant="body1" style={{ marginRight: '40px' }}>
                                        ठेगाना: &ensp;&ensp;&ensp; &ensp;&ensp;&ensp;
                                </Typography>
                                    <BootstrapInput fullWidth type="text"
                                        id="address"
                                        placeholder="ठेगना नेपालीमा"
                                        onChange={(e) => { this.setState({ address: e.target.value }) }}
                                    />
                                </Toolbar>
                            </Grid>
                            <Grid style={{ fontSize: '16px' }}
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '31px' }}>
                                            सम्पर्क नम्बर :&ensp;&ensp;
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput type="text"
                                                id="phone_number"
                                                style={{ width: 260 }}
                                                placeholder="सम्पर्क नम्बर अङ्रेजीमा"
                                                value={this.state.phone_number}
                                                onChange={(e) => { this.setState({ phone_number: e.target.value.replace(/[\D\s\._\-]+/g, "") }) }}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            ईमेल:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput type="text"
                                                id="email"
                                                onChange={this.handelemailchange}
                                                lable='ईमेल'
                                                style={{ width: 260 }}
                                                placeholder="ईमेल अङ्रेजीमा"
                                                value={this.state.email}
                                                helperText={this.state.emailVerification ? <a style={{ color: "red" }} >अवैध ईमेल</a> : ""}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </Grid>

                            <Grid style={{ fontSize: '16px' }}
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            Position:&ensp;&ensp;&ensp;
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput
                                                id="post_english"
                                                name="post_english"
                                                style={{ width: 260 }}
                                                placeholder="पद अङ्रेजीमा"
                                                value={this.state.post_english}
                                                onChange={(e) => { this.setState({ post_english: e.target.value }) }}
                                                type="text" />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            पद:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput
                                                id="post"
                                                name="post"
                                                style={{ width: 260 }}
                                                placeholder="पद नेपालीमा"
                                                value={this.state.post}
                                                type="text"
                                                onChange={(e) => { this.setState({ post: e.target.value }) }}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </Grid>

                            <Grid style={{ fontSize: '16px' }}
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            शैक्षिक योग्यता:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="educational_qualification"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={['एस.एल. सि. वा बराबर', '+२ तह वा बराबर', 'स्नातक तह वा बराबर', 'स्नातकोत्तर वा बराबर', 'पीएचडी वा बराबर']}
                                                inputValue={this.state.educational_qualification}
                                                value={this.state.educational_qualification}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ educational_qualification: newInputValue })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="शैक्षिक योग्यता छान्नुहोस्"  {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />

                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '60px' }}>
                                            समावेशिता:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="inclusion"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={['दलित', 'आदिवासी/जनजाति', 'मधेसी', 'अपाङ्ग', 'अन्य']}
                                                inputValue={this.state.inclusion}
                                                value={this.state.inclusion}
                                                getOptionDisabled={(option) => option === 'समावेशिता'}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ inclusion: newInputValue })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="समावेशिता छान्नुहोस्"  {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </Grid>

                            <Grid style={{ fontSize: '16px' }}
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '12px' }}>
                                            नागरिकता नम्बर :&ensp;&ensp;
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput type="text"
                                                id="citizenship_number"
                                                style={{ width: 260 }}
                                                placeholder="नागरिकता नम्बर अङ्रेजीमा"
                                                value={this.state.citizenship_number}
                                                onChange={(e) => { this.setState({ citizenship_number: e.target.value.replace(/[\D\s\._\-]+/g, "") }) }}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '7px' }}>
                                            नागरिकता जारी जिल्ला:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="citizenship_issued_district"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={addressData.alldistrict}
                                                value={this.state.citizenship_issued_district}
                                                getOptionDisabled={(option) => option === 'समावेशिता'}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ citizenship_issued_district: newInputValue })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="जिल्ला छान्नुहोस्"  {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </Grid>
                            <br />
                        </Box>
                        {/* Institutional Information */}
                        <Box width="100%" style={{ marginTop: '60px', border: '1.5px solid rgb(112 112 112 / 55%)' }}>
                            <Typography variant="h5" style={{
                                position: "relative",
                                top: "-11px",
                                background: "#fafafa",
                                width: "max-content",
                                left: "20px",
                                paddingLeft: "5px",
                                paddingRight: " 5px"
                            }} noWrap>
                                Institutional Information(संस्थागत विवरण)
                        </Typography>
                            <Grid style={{ fontSize: '16px', marginRight: '120px' }}>
                                <Toolbar>
                                    <Typography variant="body1" style={{ marginRight: '40px' }}>
                                        संस्थाको नाम:
                                </Typography>
                                    <BootstrapInput fullWidth placeholder="नेपालीमा" type="text"
                                        id="trainee_institution"
                                        name='trainee_institution'
                                        value={this.state.trainee_institution}
                                        onChange={this.handleTextChange}

                                    />
                                </Toolbar>
                            </Grid>
                            <Grid style={{ fontSize: '16px', marginRight: '120px' }}>
                                <Toolbar>
                                    <Typography variant="body1" >
                                        Institution Name:
                                </Typography>
                                    <BootstrapInput fullWidth type="text"
                                        placeholder=" संस्थाको नाम अङ्रेजीमा"
                                        id="trainee_institution_english"
                                        name='trainee_institution_english'
                                        value={this.state.trainee_institution_english}
                                        onChange={this.handleTextChange}

                                    />
                                </Toolbar>
                            </Grid>

                            <Grid style={{ fontSize: '16px' }}
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            प्रदेश:&ensp;&ensp;&ensp;&ensp;
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="province"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={this.state.provinces}
                                                inputValue={this.state.province ? this.state.province : null}
                                                value={this.state.province}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ province: newInputValue, provinceIndex: addressData.provinces.np.indexOf(newInputValue), districts: this.getDistricts(newInputValue), district: '', municipality: '' })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="प्रदेश छान्नुहोस्"   {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            जिल्ला:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="district"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={this.state.districts}
                                                inputValue={this.state.district ? this.state.district : null}
                                                value={this.state.district}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ district: newInputValue, municipalities: this.getMunicipalities(newInputValue), municipality: '' })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="जिल्ला छान्नुहोस्"   {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                            </Grid>

                            <Grid
                                justify="space-between" // Add it here :)
                                container
                                spacing={5}>

                                <Grid item>
                                    <Toolbar style={{ fontSize: '16px' }}>
                                        <Typography variant="body1" style={{ marginRight: '33px' }}>
                                            पालिका:&ensp;&ensp;&ensp;
                                    </Typography>
                                        <Typography variant="body1" color="inherit" noWrap >
                                            <Autocomplete
                                                id="palika"
                                                // disableClearable
                                                freeSolo
                                                autoHighlight
                                                autoComplete
                                                autoSelect
                                                options={this.state.municipalities}
                                                inputValue={this.state.municipality ? this.state.municipality : null}
                                                value={this.state.municipality}
                                                getOptionLabel={(option) => option}
                                                onInputChange={(event, newInputValue) => this.setState({ municipality: newInputValue })}
                                                renderInput={(params) => (
                                                    <div ref={params.InputProps.ref}>
                                                        <BootstrapInput style={{ width: 260 }}
                                                            placeholder="पालिका छान्नुहोस्"   {...params}  {...params.inputProps} fullWidth />
                                                    </div>
                                                )}
                                            />
                                        </Typography>
                                    </Toolbar>
                                </Grid>
                                <Grid item style={{ marginRight: '120px' }}>
                                    <Toolbar style={{ fontSize: '16px' }}>
                                        <Typography variant="body1" style={{ marginRight: '40px' }}>
                                            वडा नम्बर:
                                    </Typography>

                                        <Typography variant="body1" color="inherit" noWrap >
                                            <BootstrapInput
                                                type="text"
                                                id="ward_number"
                                                label="वार्ड नम्बर"
                                                name='ward_number'
                                                style={{ width: 260 }}
                                                placeholder="वार्ड नम्बर"
                                                value={this.state.ward_number}
                                                onChange={this.handleTextChange}
                                            />
                                        </Typography>

                                    </Toolbar>
                                </Grid>
                            </Grid>


                        </Box>

                        <Box width="100%" style={{ marginTop: '60px', border: '1.5px solid rgb(112 112 112 / 55%)' }}>
                            <Typography variant="h5" style={{
                                position: "relative",
                                top: "-11px",
                                background: "#fafafa",
                                width: "max-content",
                                left: "20px",
                                paddingLeft: "5px",
                                paddingRight: " 5px"
                            }} noWrap>
                                Document(कागजात)
                        </Typography>
                            <Grid>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="subtitle1">
                                            Citizenship (नागरिकता)
                                    </Typography>
                                        <input accept="image/*" style={{ width: '200px', display: 'none' }} onChange={(e) => this.handlePatraChange(e, 'nagarikta')} className={this.props.classes.input} id="imgsig-button-file" type="file" />
                                        <label htmlFor="imgsig-button-file">
                                            <IconButton color="primary" aria-label="upload picture" component="span">

                                                {this.state.nagariktaPatraUrl !== '' && this.state.nagariktaPatraType !== 'application/pdf' ?
                                                    <img style={{ height: 100, width: 100, float: 'right' }} src={this.state.nagariktaPatraUrl} /> :
                                                    <PhotoCamera />
                                                }
                                                {
                                                    this.state.nagariktaPatraUrl !== '' && this.state.nagariktaPatraType === 'application/pdf' &&
                                                    <>
                                                        {this.state.nagariktaPatraName}
                                                    </>
                                                }
                                            </IconButton>
                                        </label>
                                    </Toolbar>

                                </Grid>
                            </Grid>
                            <Grid container spacing={5}>
                                <Grid item>
                                    <Toolbar>
                                        <Typography variant="subtitle1">
                                            Approval Letter (मनोनयन पत्र)
                                    </Typography>
                                        <input accept="image/*" style={{ width: '200px', display: 'none' }} onChange={this.handlePatraChange} className={this.props.classes.input} id="sig-button-file" type="file" />
                                        <label htmlFor="sig-button-file">
                                            <IconButton color="primary" aria-label="upload picture" component="span">
                                                {this.state.mahoitPatraUrl !== '' && this.state.mahoitPatraType !== 'application/pdf' ?
                                                    <img style={{ height: 100, width: 100, float: 'right' }} src={this.state.mahoitPatraUrl} /> :
                                                    <PhotoCamera />
                                                }
                                                {
                                                    this.state.mahoitPatraUrl !== '' && this.state.mahoitPatraType === 'application/pdf' &&
                                                    <>
                                                        {this.state.mahoitPatraName}
                                                    </>
                                                }
                                            </IconButton>
                                        </label>
                                    </Toolbar>

                                </Grid>
                            </Grid>
                        </Box>

                        <Hoc>
                            <div id="recaptcha" />
                            <Toolbar>
                                <FirebasePhoneAuthentication validateData={this.validateData} loading={this.state.loading} showLoader={this.triggerLoad} hideLoader={this.triggerStopLoad} submit={this.handelSubmit} phoneNumber={this.state.phone_number} />
                                {this.state.loading && <CircularProgress />}
                            </Toolbar>


                        </Hoc>
                        <Copyright />
                    </main>
                </React.Fragment>
        );
    }


}

export default withStyles(useStyles)(TraineeRegistrationForm)


