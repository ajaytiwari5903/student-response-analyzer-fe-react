import React, { Fragment } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from "react-redux";
import Skeleton from '@material-ui/lab/Skeleton';
import Typography from '@material-ui/core/Typography';
import {
  withStyles,
} from '@material-ui/core/styles';
import Hoc from '../../hoc/hoc';
import { compose } from 'redux';
import { Grid, Card, CardContent, Button, Divider } from '@material-ui/core';
import { getBookAuthor } from '../../store/actions/book'
import { authCheckState } from '../../store/actions/auth';

import Chart from 'react-apexcharts';
const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '44ch'
    },
  },
  DateField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
});
class CustomSkeleton extends React.Component {



  render() {
    return (
      <Hoc>
        <Skeleton animation="wave" height={30} style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={40} />
        <Skeleton animation="wave" height={20} />
        <Skeleton animation="wave" height={40} style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={30} />
        <Skeleton animation="wave" height={30} style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={40} />
        <Skeleton animation="wave" height={20} />
        <Skeleton animation="wave" height={40} style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={30} />
        <Skeleton animation="wave" height={30} style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={40} />
        {/* <Skeleton animation="wave" height={10} /> */}

      </Hoc>
    );
  }
}

export default CustomSkeleton;
