import React from 'react';
const { Component } = React;
class Level extends React.Component {
  render() {
    const {
      levelName,
      animate,
      percentage
    } = this.props;
    return (
      <div className="tube-wrapper">
        <div className="tube">
          <div className={"liquid " + levelName + ' ' + animate} style={{ top: (100 - percentage) + '%' }}>
            <div className="bubble circle"></div>
            <div className="bubble circle2"></div>
            <div className="bubble circle3"></div>
            <div className="bubble circle4"></div>
            <div className="bubble circle5"></div>
            <div className="bubble circle6"></div>
            <div className="bubble circle7"></div>
            <div className="bubble circle8"></div>
            <div className="bubble circle9"></div>
          </div>
          <div className="metrics">
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
            <div className="line"></div>
          </div>
          <div className="side-line"></div>
          <div className="neck"></div>
          <div className="head"></div>
        </div>
        <span className="level-name">{this.props.levelName}</span>
      </div>
    )
  }
};

export default class saurav extends Component {
  state = {
    feelingIndex: 0,
    animate: '',
    current: 0
  };

  getFeelings = () => [{
    feeling: 'love',
    levels: {
      dopamine: 93,
      serotonin: 93,
      oxytocin: 93
    }
  }, {
    feeling: 'depression',
    levels: {
      dopamine: 28,
      serotonin: 14,
      oxytocin: 3
    }
  }, {
    feeling: 'happiness',
    levels: {
      dopamine: 3,
      serotonin: 100,
      oxytocin: 7
    }
  }, {
    feeling: 'anxiety',
    levels: {
      dopamine: 20,
      serotonin: 3,
      oxytocin: 3
    }
  }];

  changeFeeling = () => {
    let start = Date.now();
    this.setState({
      feelingIndex: this.state.current,
      animate: 'animate'
    });
    this.setState({
      current: this.state.current + 1
    });
    if (this.state.current === this.getFeelings().length) {
      this.setState({
        current: 0
      });
    }
    setTimeout(() => {
      let end = Date.now() - start;
      if (end > 3950) {
        this.setState({
          animate: ''
        });
      }
    }, 4000)
  };

  setInterval = () => {
    this.changeFeeling();
    setInterval(this.changeFeeling, 4500);
  };

  componentDidMount() {
    this.setInterval();
  };

  render() {
    const {
      feelingIndex,
      animate
    } = this.state;
    const feeling = this.getFeelings()[feelingIndex];
    const feelingName = feeling.feeling;
    const levels = feeling.levels;
    return (
      <div className="App">
        <h1 className={animate}>{feelingName}</h1>
        <div className="tubes-wrapper" >
          {Object.keys(levels).map((level, index) => <Level key={index} animate={animate} levelName={level} percentage={levels[level]}></Level>)}
        </div>
        <a className="source-link" target="_blank" href="https://www.facebook.com/sauravthakurofficial/" style={{marginTop:-20}}>Saurav Thakur</a>
      </div>
    );
  }
}
