import React from 'react'
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import {Button} from "@material-ui/core";
import DialogContent from "@material-ui/core/DialogContent";
import Slide from "@material-ui/core/Slide";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
const useStyles = (theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '44ch'
        },
    },
    DateField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
});
class BatchDialog extends React.Component{

    state = {
        open:false
    }

    render() {
        const {classes,printHtml,handleClose} = this.props

        const handlePrint = () => {
            let w = window.open()

            w.document.write(printHtml)
            w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')

            w.document.close() // necessary for IE >= 10
            w.focus() //
            handleClose()
        }

        return (<Dialog fullScreen open={this.props.open} onClose={handleClose} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        Print Data
                    </Typography>
                    <Button autoFocus color="inherit" onClick={handlePrint}>
                        Print
                    </Button>
                </Toolbar>
            </AppBar>

            <DialogContent>
                <div style={{ marginTop: '80px', zIndex: 999999 }} dangerouslySetInnerHTML={{ __html: printHtml }}>

                </div>
            </DialogContent>
        </Dialog>)
    }
}
export default BatchDialog