import React, { Fragment } from 'react';
import {
    Button,
    Checkbox,
    Divider,
    FormControlLabel,
    TextField,
    MenuItem
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
const validate = (values, requiredfields) => {
    const errors = {}
    const requiredFields = requiredfields
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

export class LoginForm extends React.Component {
    state = {
        username: '',
        password: '',
        showPassword: false
    }

    render() {
        const handelSubmit = (e) => {
            e.preventDefault();
            const { username, password } = this.state
            this.props.onAuthLogin(username, password)
        }

        const { classes } = this.props;
        const { error } = this.props;
        const { successAuth } = this.props;
        if (successAuth) {
            this.props.history.push("/admin/DashboardDefault");
        }
        return (
            <Card variant="outlined" style={{ maxWidth: "540px" }}>
                <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>LogIn</div>
                {
                    error
                    &&
                    <Alert severity="warning" style={{ padding: '5px', }}>Your credentials could not be verified</Alert>
                }
                <Divider className="my-2" />
                <CardContent style={{ padding: 5 }} className="d-flex flex-column">
                    <form onSubmit={handelSubmit} className={classes.root} noValidate autoComplete="off">
                        <FormControlLabel
                            control={
                                <TextField id="username" label="User Name" type='text' name='username' onChange={(e) => { this.setState({ username: e.target.value }) }} autoFocus />
                            }
                            lable='User Name'
                        />
                        <br />
                        <FormControlLabel
                            control={
                                this.state.showPassword ?
                                    <TextField id="password" label="Password" type='text' name='password' onChange={(e) => { this.setState({ password: e.target.value }) }} />
                                    :
                                    <TextField id="password" label="Password" type='password' name='password' onChange={(e) => { this.setState({ password: e.target.value }) }} />
                            }
                            lable='Password'
                        />
                        <br />
                        <FormControlLabel
                            style={{}}
                            control={
                                <Checkbox
                                    onChange={(e) => { this.setState({ showPassword: !this.state.showPassword }) }}
                                    value="checked"
                                />
                            }
                            label="Show Password"
                        />
                        <br />
                        <Button
                            // to="/admin/DashboardDefault"
                            // component={Link}
                            type='submit'
                            style={{ marginTop: '12px' }}
                            variant="outlined"
                        >LogIn</Button>

                        <Link to='/admin/auth/signup' style={{ color: 'red' }}>SignUp</Link>
                    </form>
                </CardContent>
            </Card>
        )
    }
}



export class SignUpForm extends React.Component {
    state = {
        name: '',
        password1: '',
        password2: '',
        showPassword: false,
        address: '',
        position: '',
        email: '',
        pnumber: '',
        gender: '',
        emailVerification: false,
        passwordDidNotMatch: false,
    }

    render() {
        const errors = {}
        const handelSubmit = (e) => {
            e.preventDefault();
            console.log(this.state)
            if (this.state.password1 == this.state.password2) {
                this.props.onAuthSignUp(this.state.name, this.state.address, this.state.email, this.state.password1, this.state.password2, this.state.pnumber, this.state.position, this.state.gender)
            }
            else {
                this.setState({ passwordDidNotMatch: true })
            }
        }
        const handelemailchange = (e) => {
            this.setState({ email: e.target.value })
            if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(e.target.value)) {
                this.setState({ emailVerification: true })
            }
            else {
                this.setState({ emailVerification: false })
            }
        }
        const { classes } = this.props;
        const { error } = this.props;
        const { successAuth } = this.props;
        if (successAuth) {
            this.props.history.push("/admin/DashboardDefault");
        }
        return (
            <Card variant="outlined" style={{ maxWidth: "700px" }}>
                <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>SignUp</div>
                {
                    error
                    &&
                    <Alert severity="warning" style={{ padding: '5px', }}>Your credentials could not be verified</Alert>
                }
                {
                    this.state.passwordDidNotMatch &&
                    <Alert severity="warning" style={{ padding: '5px', }}>Your Password Did Not Match</Alert>
                }
                <Divider className="my-2" />
                <CardContent style={{ padding: 5 }}>
                    <form onSubmit={handelSubmit} className={classes.root} noValidate autoComplete="off">

                        <FormControlLabel
                            style={{ color: 'red' }}
                            control={
                                <TextField id="name" label="Name" type='text' name='name' onChange={(e) => { this.setState({ name: e.target.value }) }} />
                            }
                            lable={errors['name'] ? "Name is Required" : "Name"}
                        />
                        <FormControlLabel
                            control={
                                <TextField id="address" label="adderss" type='adderss' name='adderss' onChange={(e) => { this.setState({ address: e.target.value }) }} />
                            }
                            lable='Address'
                        />
                        <FormControlLabel
                            control={
                                <TextField id="email" label="Email" type='email' name='email' onChange={handelemailchange}
                                    lable='Email'
                                    helperText={this.state.emailVerification ? "Invalid Email" : ""}
                                />
                            }
                        />
                        <FormControlLabel
                            control={
                                this.state.showPassword ?
                                    <TextField id="password1" label="Password" type='text' name='password1' onChange={(e) => { this.setState({ password1: e.target.value }) }} />
                                    :
                                    <TextField id="password1" label="Password" type='password' name='password1' onChange={(e) => { this.setState({ password1: e.target.value }) }} />
                            }

                        />
                        <FormControlLabel
                            control={
                                this.state.showPassword ?
                                    <TextField id="password2" label="Password Again" type='text' name='password2' onChange={(e) => { this.setState({ password2: e.target.value }) }} />
                                    :
                                    <TextField id="password2" label="Password Again" type='password' name='password2' onChange={(e) => { this.setState({ password2: e.target.value }) }} />
                            }

                        />
                        <FormControlLabel
                            style={{}}
                            control={
                                <Checkbox
                                    onChange={(e) => { this.setState({ showPassword: !this.state.showPassword }) }}
                                    value="checked"
                                />
                            }
                            label="Show Password"
                        />


                        <FormControlLabel
                            control={
                                <TextField id="" label="Phone Number" type='number' name='pnumber' onChange={(e) => { this.setState({ pnumber: e.target.value }) }} />
                            }
                            lable='Phone Number'
                        />


                        <FormControlLabel
                            control={

                                <TextField id="position" label="Position" type='email' name='position' onChange={(e) => { this.setState({ position: e.target.value }); }} />
                            }
                            lable='Position'
                        />

                        <TextField
                            select

                            onChange={(e) => { this.setState({ gender: e.target.value }) }}
                            label='Gender'
                        >
                            {['Male', 'Female'].map(option => (
                                <MenuItem key={option} value={option}>
                                    {option}
                                </MenuItem>
                            ))}
                        </TextField>

                        <Button
                            to="/admin/DashboardDefault"
                            // component={Link}
                            type='submit'
                            style={{ marginTop: '12px' }}
                            variant="outlined"
                        >SignUp</Button>
                        <Link to='/admin/auth/login' style={{ color: 'red' }}>LogIn</Link>
                    </form>
                </CardContent>
            </Card>
        )
    }
}


