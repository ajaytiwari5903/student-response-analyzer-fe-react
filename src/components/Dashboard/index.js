import React, { Fragment } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { Grid, Card, CardContent, Button, Divider } from "@material-ui/core";

import Chart from "react-apexcharts";
import axios from "axios";
import { authCheckState } from "../../store/actions/auth";
import { mainLink } from "../../store/utils";
import { compose } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
const useStyles = (theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "100ch",
    },
  },
});
class DashBoard extends React.Component {
  state = {
    loading: false,
    data: [],
  };
  componentDidMount() {
    this.props.authCheckState();
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };

    this.setState({ loading: true });
    axios
      .get(window.location.origin + `/api/v1/dashboardData/`)
      .then((res) => {
        let data = res.data;
        console.log(data);
        data.map((item, index) => {
          if (typeof item == "string") {
            console.log(typeof item);
            console.log(JSON.parse(item));
            // item = JSON.parse(item.replace('"', " "));
            console.log(item);
          }
        });
        this.setState({ loading: false, data: data });
      })
      .catch((err) => {
        this.setState({ loading: false });

        console.log(err);
        this.setState({
          isErrorDialogPopUp: true,
          ErrorMessage: err.message,
        });
      });
  }
  render() {
    const chart30Data = [
      {
        name: "Customers",
        data: [47, 38, 56, 24, 45, 54, 38, 47, 38, 56, 24, 56, 24, 65],
      },
      {
        name: "Customers",
        data: [47, 38, 56, 24, 45, 54, 38, 47, 38, 56, 24, 56, 24, 65],
      },
    ];
    const chart30Options = {
      chart: {
        type: "donut",
      },
      labels: ["Cluster 1", "Cluster 2"],
    };

    return (
      <Fragment>
        {this.state.loading ? (
          <div style={{ textAlign: "center" }}>Loading Please Wait</div>
        ) : (
          <div>
            {this.state.data != undefined &&
              this.state.data != null &&
              this.state.data.map((item, index) => {
                return (
                  <div
                    key={index}
                    style={{ backgroundColor: "white", padding: 20 }}
                  >
                    <div>{item.question}</div>
                    <div style={{ padding: 10 }}>
                      <span style={{ marginRight: 10 }}>Summary :</span>
                      {item.answer_summ}
                    </div>
                  </div>
                );
              })}
          </div>
        )}
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    userId: state.auth.userId,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authCheckState: () => dispatch(authCheckState()),
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(DashBoard);
