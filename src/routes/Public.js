import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { AnimatePresence, motion } from 'framer-motion';

import { ThemeProvider } from '@material-ui/styles';

import MuiTheme from '../theme';
import BatchForm from '../pages/Public/Dashboard/form'

// Layout Blueprints

import { PublicIndex } from '../main';

// Example Pages


const PublicDashboard = lazy(() => import('../pages/Public/Dashboard'));

const Routes = () => {
    const location = useLocation();

    const pageVariants = {
        initial: {
            opacity: 0,
            scale: 0.99
        },
        in: {
            opacity: 1,
            scale: 1
        },
        out: {
            opacity: 0,
            scale: 1.01
        }
    };

    const pageTransition = {
        type: 'tween',
        ease: 'anticipate',
        duration: 0.4
    };

    return (
        <ThemeProvider theme={MuiTheme}>
            <AnimatePresence>
                <Suspense
                    fallback={
                        <div className="d-flex align-items-center vh-100 justify-content-center text-center font-weight-bold font-size-lg py-3">
                            <div className="w-50 mx-auto">
                                Loading
                            </div>
                        </div>
                    }>
                    <Switch>
                        <Redirect exact from="/" to="/public" />
                        <Redirect exact from="/login" to="/admin/auth/login" />

                        <Route
                            path={[
                                '/public',
                                '/public/Batch-public/:id',
                                '/public/about-us'
                            ]}>
                            <PublicIndex>
                                <Switch location={location} key={location.pathname}>
                                    <motion.div
                                        initial="initial"
                                        animate="in"
                                        exit="out"
                                        variants={pageVariants}
                                        transition={pageTransition}>
                                        <Route
                                            exact path="/public"
                                            name='public-dash'
                                            component={PublicDashboard}
                                        />

                                        <Route
                                            name='public-batch-form'
                                            exact path='/public/Batch-public/:id'
                                            component={BatchForm}
                                        />
                                    </motion.div>
                                </Switch>
                            </PublicIndex>
                        </Route>
                    </Switch>
                </Suspense>
            </AnimatePresence>
        </ThemeProvider>
    );
};

export default Routes;
