import { Route } from "react-router-dom";
import React from "react";
import * as actions from "../store/actions/auth";
import { connect } from "react-redux";

const RouteMiddleware = (props) => {
    if (!props.permissions || props.permissions.indexOf(props.name) >= 0) {
        return (<Route {...props} />)
    }
    return (<Route {...props} />)

    // return (<Route path={props.path} exact render={() => <div>Sorry, you are not authorized</div>} />)


}

const mapStateToProps = state => {
    console.log(state.auth.user.permissions)
    return {
        isAuthenticated: state.auth.token !== null,
        permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch(actions.authCheckState())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RouteMiddleware);