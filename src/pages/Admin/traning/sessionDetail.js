import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import MaterialTable from 'material-table';
import { PageTitle } from '../../../layout/Admin';
import { authCheckState } from '../../../store/actions/auth';
import axios from 'axios';
import { MTableToolbar } from 'material-table'
import { connect } from "react-redux";
import { Paper, Icon, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import Hoc from '../../../hoc/hoc';
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import { updateTrainingInfo, clearTrainingInfo } from '../../../store/actions/auth';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Loading from '../../../components/Loading/index';
import dateMap from "../traning/date";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});


class Component extends React.Component {
  state = {
    data: [],
    training: '',
    formDisplay: false,
    loading: false,
    sayojug: '',
    saha_sayojug: '',
    prabidik_karmachari: '',
    karyalaya_sahayogi: '',
  }
  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .get(window.location.origin + `/api/v1/trainingwisesession/${this.props.match.params.id}/`)
      .then(res => {
        this.setState({ loading: false })
        let data = res.data.data
        console.log(data)
        console.log(data[0]['date'])
        for (let i = 0; i < data.length; i++) {
          data[i]['date'] = data[i]['date'] in dateMap ? dateMap[data[i]['date']] : data[i]['date'];
          // console.log(data[i]['date'])
        }

        this.setState({
          data: res.data.data,
          training: res.data.training,

        })
        let trainingDate = {
          'comittee': res.data.comittee,
          'training': res.data.training
        }
        this.props.updateTrainingInfo(trainingDate)
      }
      )
      .catch(err => {
        this.setState({ loading: false })

        console.log(err)
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


      });

  }
  componentWillUnmount() {
    this.props.clearTrainingInfo()
  }

  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true })
      console.log(this.state)
    }
    console.log(JSON.parse(localStorage.getItem('categorys')))
    console.log(JSON.parse(localStorage.getItem('categorys')))
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    return (
      <Fragment>
        {this.state.loading ?

          <Loading /> :
          (
            <Hoc>
              <Dialog
                open={this.state.isErrorDialogPopUp}
                onClose={handleErrorClose}
                TransitionComponent={Transition}
                // PaperComponent={PaperComponent}
                aria-labelledby="draggable-dialog-title"
              >
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                  Error Message
              </DialogTitle>
                <DialogContent>
                  <DialogContentText>
                    <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleErrorClose} color="primary" autoFocus>
                    Ok
                </Button>
                </DialogActions>
              </Dialog>
              <MaterialTable
                title={
                  <div>

                    <div style={{ fontSize: 20, fontWeight: 'bold', marginTop: 5 }}>प्रशिक्षणको नाम : {this.state.training && this.state.training}</div>
                    <div style={{ fontSize: 17, fontWeight: 'bold' }}>
                      सत्रहरू</div>
                  </div>
                }
                columns={[
                  {
                    title: 'परिचय नम्बर',
                    field: 'serial_number',
                    emptyValue: "",
                  },
                  {
                    title: 'नाम',
                    field: 'name',
                    emptyValue: "",
                    render: rowData => <a>{rowData.name.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.name}</a>

                  },
                  {
                    title: 'रशिक्षक',
                    field: 'trainer_converted',
                    emptyValue: "",
                  },
                  {
                    title: 'अवधि',
                    field: 'duration',
                    emptyValue: "",
                  },
                  {
                    title: 'मिति',
                    field: 'date',
                    emptyValue: "",
                  }
                ]}
                data={this.state.data}
                options={
                  {
                    // filtering: true,
                    actionsColumnIndex: -1,
                    emptyRowsWhenPaging: false,
                    pageSize: 20,
                    pageSizeOptions: [20, 50, 100]
                  }
                }
                editable={{
                  onRowDelete: oldData =>
                    new Promise((resolve, reject) => {
                      const dataDelete = this.state.data;
                      const index = oldData.tableData.id;
                      this.setState({ loading: true })

                      axios
                        .delete(window.location.origin + `/api/v1/session/${oldData.id}/`)
                        .then(res => {
                          dataDelete.splice(index, 1);
                          this.setState([...dataDelete]);
                          this.setState({ loading: false })
                        }
                        )
                        .catch(err => {
                          this.setState({ loading: false })

                          console.log(err)
                          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

                        })
                      console.log(index)
                      resolve();
                    })
                }}
                actions={[

                  {
                    icon: 'edit',
                    tooltip: 'Edit',
                    onClick: (event, rowData) =>
                      this.props.history.push(`/admin/training/${this.props.match.params.id}/session-edit/${rowData.id}`)


                  },
                  {
                    icon: 'assessment',
                    tooltip: 'हाजिर',
                    onClick: (event, rowData) =>
                      this.props.history.push(`/admin/training/${this.props.match.params.id}/session-attendance/${rowData.id}`)


                  },
                ]}
                localization={{
                  toolbar: {
                    searchTooltip: 'खोजी गर्नुहोस्',
                    searchPlaceholder: 'खोजी गर्नुहोस्'
                  },
                  header: {
                    actions: 'कार्यहरू'
                  },
                  body: {
                    emptyDataSourceMessage: 'कुनै रेकर्ड छैन',
                    filterRow: {
                      filterTooltip: 'Filter'
                    }
                  }
                }}
                components={{
                  Toolbar: props => (
                    <div>
                      <div style={{ padding: 20, paddingBottom: 0 }}>
                        <Link label="Chip 1" color="secondary" style={{ marginRight: 5, float: 'right' }} to={`/admin/training/${this.props.match.params.id}/session-create`} >
                          <AddIcon style={{ fontSize: 40 }} />
                        </Link>
                      </div>
                      <MTableToolbar {...props} />

                    </div>
                  ),
                }}
              />
            </Hoc>

          )
        }



      </Fragment >
    )
  };
}
const mapStateToProps = state => {
  console.log(state.book.categorys)
  return {
    editions: state.book.editions,
    token: state.auth.token,
    loading: state.book.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateTrainingInfo: (data) => dispatch(updateTrainingInfo(data)),
    clearTrainingInfo: () => dispatch(clearTrainingInfo()),
    authCheckState: () => dispatch(authCheckState()),

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component);
