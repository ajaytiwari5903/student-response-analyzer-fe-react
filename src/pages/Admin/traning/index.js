import React, { Fragment } from 'react';
import MaterialTable from 'material-table';
import { authCheckState } from '../../../store/actions/auth';
import { mainLink } from '../../../store/utils'
import axios from 'axios'
import Tooltip from '@material-ui/core/Tooltip';
import Loading from '../../../components/Loading/index';
import Checkbox from '@material-ui/core/Checkbox';
import { MTableToolbar } from 'material-table'
import { connect } from "react-redux";
import { Paper, Icon, Button, Select, Switch } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import addressData from "../../../components/TraineeRegistrationForm/addressData";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Divider,
  FormControlLabel,
  TextField,
  MenuItem
} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import SchoolOutlinedIcon from '@material-ui/icons/SchoolOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import AssessmentOutlinedIcon from '@material-ui/icons/AssessmentOutlined';
import PrinterOutlined from "@ant-design/icons/lib/icons/PrinterOutlined";
import dateMap from './date'
import Hoc from "../../../hoc/hoc";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});





class Component extends React.Component {
  state = {
    data: [],
    trainings: [],
    formDisplay: false,
    loading: false,
    showPrintDialog: false,
    showCsvPrintDialog: false,
    showEmailDialog: false,
    downloadAllTraining: false,
    extramail: '',
    invalidMail: false,
    trainee_training: '',
    tId: '',
    certificatetype: 0,
    checkList: {},
    mailtype: 'certificate'
  }
  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .get(window.location.origin + `/api/v1/trainee/trainings/`)
      .then(res => {
        if (typeof res.data === "object" && res.data.length) {
          this.setState({
            trainings: res.data
          })
        } else {
          this.setState({
            trainings: []
          })
        }


      })
      .catch(err => {
        this.setState({
          isErrorDialogPopUp: true,
          ErrorMessage: 'Failed to load trainings'
        })
      })
    axios
      .get(mainLink + "/api/v1/traninigs/")
      .then(res => {
        this.setState({ loading: false })

        // let data = res.data.map(item => {
        //   item['certificate_issue_date_np'] = item.certificate_issue_date in dateMap ? dateMap[item.certificate_issue_date] : item.certificate_issue_date
        //   item['start_date_np'] = item.start_date in dateMap ? dateMap[item.start_date] : item.start_date
        //   item['end_date_np'] = item.end_date in dateMap ? dateMap[item.end_date] : item.end_date
        // return item
        // })
        // console.log(data)
        // this.setState({
        //   data: data
        // })
      }
      )
      .catch(err => {
        this.setState({ loading: false })

        console.log(err)
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

      });

  }
  // static getDerivedStateFromProps(nextProps, prevState,/* nextProps2,prevState2 */) {
  //   if (nextProps.editions && nextProps.editions !== prevState.data) {
  //     return { data: nextProps.editions };
  //   }
  //   else return null;
  // }

  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true })
      console.log(this.state)
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };

    const handlePrintDialogClose = (e) => {
      this.setState({
        showPrintDialog: false
      })
    }
    const handleCsvPrintDialogClose = (e) => {
      this.setState({
        showCsvPrintDialog: false

      })
    }
    const handleEmailDialogClose = (e) => {
      this.setState({
        showEmailDialog: false
      })
    }
    const handleSendMail = (e) => {
      let isValid = true
      if (this.state.extramail != '') {
        this.state.extramail.split(',').map(each => {
          if (!(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(each))) {

            isValid = isValid && false;
          }
        })
        if (!isValid) {

          this.setState({
            validateMail: 'Sorry, mail is not valid',
            invalidMail: true
          })
          return;
        } else {
          this.setState({
            validateMail: '',
            invalidMail: false
          })
        }
      }
      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      this.setState({
        loading: false,
        showEmailDialog: false,
      })
      axios
        .get(window.location.origin + `/api/v1/traninigsdetail/img_for_mail/?id=${this.state.printId}&template=${this.state.mailtype}${this.state.extramail == '' ? '' : '&email=' + this.state.extramail}`)
        .then(res => {



        }
        )
        .catch(err => {
          this.setState({
            showEmailDialog: false,
            loading: false
          })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: "माफ गर्नुहोल केही त्रुती भएको छ।" })


        });
    }
    const handlePrint = (e) => {

      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      this.setState({ loading: true })
      axios
        .get(window.location.origin + `/api/v1/traninigsdetail/certificate/?id=${this.state.printId}${this.state.certificatetype ? '&p' : ''}`)
        .then(res => {
          let data = res.data
          console.log(data)

          let w = window.open()

          w.document.write(data)
          w.document.write('<scr' + 'ipt type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</sc' + 'ript>')

          w.document.close() // necessary for IE >= 10
          w.focus() //
          this.setState({
            loading: false,
            showPrintDialog: false,
          })

        }
        )
        .catch(err => {
          this.setState({
            showPrintDialog: false,
            loading: false
          })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: "माफ गर्नुहोल केही त्रुती भएको छ।" })


        });
    }
    const downloadFile = (fileName, urlData) => {

      var aLink = document.createElement('a');
      var evt = document.createEvent("HTMLEvents");
      evt.initEvent("click");
      aLink.download = fileName;
      aLink.href = urlData;
      aLink.dispatchEvent(evt);
    }
    const handlePrintCsv = async (e) => {
      if (!this.state.trainee_training && !this.state.downloadAllTraining) {
        this.setState({ isErrorDialogPopUp: true, ErrorMessage: "कृपया प्रशिक्षण चयन गर्नुहोस्" })
      } else {
        axios.defaults.headers = {
          "Content-Type": "application/json",
          Authorization: `Token ${this.props.token}`,

        };
        axios.defaults.params = {
          response_type: 'arraybuffer'
        }
        let data = this.state.downloadAllTraining ? 'all' : this.state.tId
        // this.setState({ loading: true })
        // let response = await axios.get(window.location.origin + `/api/v1/trainee/downloadTraining/`, { params: {data} })
        // let blob = new Blob([response.data], { type: 'application/zip' })
        // console.log(response)
        let link = document.createElement('a')
        // link.href = window.URL.createObjectURL(blob)
        var env = process.env.NODE_ENV || 'development';
        console.log(window.location.origin);
        // link.href = `http://localhost:8000/api/v1/trainee/downloadTraining/?data=${data}`
        link.href = window.location.origin + `/api/v1/trainee/downloadTraining/?data=${data}`
        link.target = '_blank';
        link.click()
        this.setState({ showCsvPrintDialog: false })
        // let test = document.createElement('a')
        // // link.href = window.URL.createObjectURL(blob)
        // var env = process.env.NODE_ENV || 'development';
        // console.log(env);
        // test.href = `http://localhost:8000/api/v1/trainee/downloadTraining/?data=${'clear'}`
        // test.target = '_blank';
        // test.click()
      }
    }
    const handleChangeCertificate = (e) => {
      this.setState({
        certificatetype: e.target.value
      })
    }
    const handleChangeMailType = (e) => {
      this.setState({
        mailtype: e.target.value
      })
    }
    const handleSearchChange = (search) => {
      console.log(search);
    }
    return (
      this.state.loading ?
        <Fragment>
          <Loading />
        </Fragment>

        :
        <Fragment>
          <Dialog
            open={this.state.showCsvPrintDialog}
            onClose={handleCsvPrintDialogClose}
            style={{ marginLeft: 0 }}

            TransitionComponent={Transition}
            aria-labelledby="draggable-dialog-title"
          >
            <DialogTitle>प्रशिक्षण छनौट गर्नुहोस्</DialogTitle>
            <FormControlLabel
              style={{ marginLeft: 0 }}
              control={
                <Checkbox
                  defaultChecked
                  color="primary"
                  style={{ marginLeft: 0 }}
                  checked={this.state.downloadAllTraining}
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                  onChange={(e) => { this.setState({ downloadAllTraining: !this.state.downloadAllTraining }) }}
                />
              }
              label="सबै डाउनलोड गर्नुहोस्"

            />
            <Autocomplete
              id="trainee_training"
              // disableClearable
              freeSolo
              style={{ width: "290px", paddingLeft: '20px' }}


              autoHighlight
              autoComplete
              options={this.state.trainings}
              inputValue={this.state.training}

              getOptionDisabled={(option) => option.disabled}
              getOptionLabel={(option) => option.name}
              onInputChange={(event, newInputValue) => {
                if (this.state.downloadAllTraining) {
                  this.setState({ downloadAllTraining: false })
                }
                var data = this.state.trainings.filter(item => item.name === newInputValue)
                if (data.length) {
                  this.setState({
                    training: newInputValue,
                    trainee_training: newInputValue,
                    tId: data[0].id
                  })
                } else {
                  this.setState({
                    training: newInputValue
                  })
                }
              }
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  id="trainee_training"
                  label="प्रशिक्षण" type='text' name='trainee_training'
                  style={{ width: "100%" }}
                // InputProps={{ ...params.InputProps, type: 'search' }}
                />
              )}
            />
            <DialogActions>
              <Button onClick={handlePrintCsv} color="primary" autoFocus>
                Ok
              </Button>
              <Button onClick={handleCsvPrintDialogClose} color="primary" autoFocus>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            open={this.state.showPrintDialog}
            onClose={handlePrintDialogClose}
            TransitionComponent={Transition}
            aria-labelledby="draggable-dialog-title"
          >

            <DialogTitle>प्रमाण-पत्र / प्रशंसा-पत्र</DialogTitle>
            <DialogContent>
              <Select
                value={this.state.certificatetype}
                onChange={handleChangeCertificate}
              >
                <MenuItem value={0}>प्रमाण-पत्र</MenuItem>
                <MenuItem value={1}>प्रशंसा-पत्र</MenuItem>
              </Select>
            </DialogContent>
            <DialogActions>
              <Button onClick={handlePrint} color="primary" autoFocus>
                Ok
              </Button>
              <Button onClick={handlePrintDialogClose} color="primary" autoFocus>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            open={this.state.showEmailDialog}
            onClose={handleEmailDialogClose}
            TransitionComponent={Transition}
            aria-labelledby="draggable-dialog-title"
          >
            <DialogTitle>मेल पठाउनुहोस्</DialogTitle>
            <Select
              value={this.state.mailtype}
              onChange={handleChangeMailType}
            >
              <MenuItem value={'general'}>सामान्य सूचना</MenuItem>
              <MenuItem value={'certificate'}>प्रमाण-पत्र</MenuItem>
            </Select>
            <DialogContent>
              <TextField
                label="Extra Mail"
                value={this.state.extramail}
                helperText={this.state.invalidMail ? <a style={{ color: "red" }} >अवैध ईमेल</a> : ""}
                onChange={e => { this.setState({ extramail: e.target.value }) }}
              >
              </TextField>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleSendMail} color="primary" autoFocus>
                Ok
              </Button>
              <Button onClick={handleEmailDialogClose} color="primary" autoFocus>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            open={this.state.isErrorDialogPopUp}
            onClose={handleErrorClose}
            TransitionComponent={Transition}
            // PaperComponent={PaperComponent}
            aria-labelledby="draggable-dialog-title"
          >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
              Error Message
              </DialogTitle>
            <DialogContent>
              <DialogContentText>
                <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleErrorClose} color="primary" autoFocus>
                Ok
                </Button>
            </DialogActions>
          </Dialog>
          <MaterialTable
            tableRef={this.tableRef}
            title={'प्रशिक्षण'}
            columns={[
              {
                title: 'परिचय नम्बर',
                field: 'serial_number',
                emptyValue: "",
              },
              {
                title: 'नाम',
                field: 'name',
                emptyValue: "",
                render: rowData => <a>{rowData.name.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.name}</a>
              },
              {
                title: 'प्रमाणपत्र जारी मिति',
                field: 'certificate_issue_date_np',
                emptyValue: "",
              },
              {
                title: 'सुरु मिति',
                field: 'start_date_np',
                emptyValue: "",
              },
              {
                title: 'अन्त्य मिति',
                field: 'end_date_np',
                emptyValue: "",
              },
              {
                title: 'दिनको संख्या',
                field: 'total_number_of_days',
                emptyValue: "",
              },
              {
                title: 'सत्र संख्या',
                field: 'total_number_of_session',
                emptyValue: "",
              },
              {
                title: 'दर्ता मा देखाउनुहोस्',
                field: 'include_in_self_registration',
                render: rowData => {
                  var list = this.state.checkList
                  if (list[rowData.id] === undefined) {
                    list[rowData.id] = rowData.include_in_self_registration
                  }
                  this.state.checkList = list;
                  return <Switch
                    checked={this.state.checkList[rowData.id]}
                    onChange={(e) => {
                      var list = this.state.checkList
                      if (list[rowData.id] === undefined) {
                        list[rowData.id] = rowData.include_in_self_registration
                      }
                      list[rowData.id] = e.target.checked
                      this.setState({
                        checkList: list
                      })
                    }}
                    name="checkedA"
                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                  />
                }
              }
            ]}

            data={
              query =>
                new Promise((resolve, reject) => {
                  let url = '/api/v1/PaginationTrainingView/?'
                  url += 'page_size=' + query.pageSize
                  url += '&page=' + (query.page + 1)
                  url += '&ordering=' + '-serial_number'
                  if (query.search.length > 0) {
                    url += '&search=' + query.search
                  }
                  console.log('print', query)
                  fetch(url)
                    .then(response => response.json())
                    .then(result => {
                      let data = result.results.map(item => {
                        item['certificate_issue_date_np'] = item.certificate_issue_date in dateMap ? dateMap[item.certificate_issue_date] : item.certificate_issue_date
                        item['start_date_np'] = item.start_date in dateMap ? dateMap[item.start_date] : item.start_date
                        item['end_date_np'] = item.end_date in dateMap ? dateMap[item.end_date] : item.end_date
                        return item
                      })
                      // this.state.data = data
                      // this.state.page = query.page * query.pageSize
                      // this.state.total = result.total_data
                      resolve({
                        data: data,
                        page: query.page,
                        totalCount: result.count,
                      })
                      window.localStorage.setItem('current_book_page', query.page)
                    })
                })
            }
            onSearchChange={handleSearchChange}
            options={
              {
                filtering: false,
                // search: false,
                exportButton: true,
                actionsColumnIndex: -1,
                emptyRowsWhenPaging: false,
                pageSize: 20,
                pageSizeOptions: [20, 50, 100],
                exportCsv: (columns, data) => {
                  this.setState({ showCsvPrintDialog: true })
                }
              }
            }
            editable={{
              onRowDelete: oldData =>
                new Promise((resolve, reject) => {
                  const dataDelete = this.state.data;
                  const index = oldData.tableData.id;
                  this.setState({ loading: true })
                  axios
                    .delete(mainLink + `/api/v1/traninigs/${oldData.id}/`)
                    .then(res => {
                      dataDelete.splice(index, 1);
                      this.setState([...dataDelete]);
                      this.setState({ loading: false })
                    }
                    )
                    .catch(err => {
                      this.setState({ loading: false })

                      console.log(err)
                      this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

                    })
                  console.log(index)
                  resolve();
                })
            }}
            actions={[
              {
                icon: () => <SchoolOutlinedIcon />,
                tooltip: 'प्रशिक्षार्थीहरु',
                onClick: (event, rowData) =>
                  this.props.history.push(`/admin/traning-detail/${rowData.id}`)


              },

              {
                icon: () => <AssessmentOutlinedIcon />,
                tooltip: 'सत्रहरू',
                onClick: (event, rowData) =>
                  this.props.history.push(`/admin/traning-session-detail/${rowData.id}`)


              },

              {
                icon: () => <EditOutlinedIcon />,
                tooltip: 'सम्पादन गर्नुहोस्',
                onClick: (event, rowData) =>
                  this.props.history.push(`/admin/traning-edit/${rowData.id}`)


              },
              {
                icon: 'print',
                tooltip: 'Print',
                onClick: (event, rowData) => {
                  this.setState({
                    printId: rowData.id,
                    showPrintDialog: true
                  })
                }
              },
              {
                icon: 'email',
                tooltip: 'Email',
                onClick: (event, rowData) => {
                  this.setState({
                    printId: rowData.id,
                    showEmailDialog: true
                  })
                }
              }
            ]}
            localization={{
              toolbar: {
                searchTooltip: 'खोजी गर्नुहोस्',
                searchPlaceholder: 'खोजी गर्नुहोस्',
                exportName: 'डाउनलोड गर्नुहोस्'
              },
              header: {
                actions: 'कार्यहरू'
              },
              body: {
                emptyDataSourceMessage: 'कुनै रेकर्ड छैन',
                filterRow: {
                  filterTooltip: 'Filter'
                }
              }
            }}
            components={{
              Toolbar: props => (
                <div>
                  <div style={{ padding: 20, paddingBottom: 0 }}>
                    <Tooltip title="प्रशिक्षक थप्नु" placement="top">
                      <Link label="Chip 1" color="secondary" style={{ marginRight: 5, float: 'right' }} to="/admin/traning-create/" >
                        {this.props.permissions.length ?
                          !this.props.permissions || this.props.permissions.indexOf('edition-add') >= 0 &&
                          <AddIcon style={{ fontSize: 40 }} /> :
                          <AddIcon style={{ fontSize: 40 }} />
                        }
                      </Link>
                    </Tooltip>
                  </div>
                  <MTableToolbar {...props} />

                </div>
              ),
              OverlayLoading: props => (<div></div>)
            }}
          />
          <Button
            variant="contained" color="primary"
            onClick={() => {
              axios.defaults.headers = {
                "Content-Type": "application/json",
                Authorization: `Token ${this.props.token}`,
              };
              this.setState({ loading: true })
              axios
                .post(window.location.origin + "/api/v1/traninigs/show_checklist/", this.state.checkList)
                .then(res => {
                  this.setState({ loading: false })
                }
                )
                .catch(err => {
                  this.setState({ loading: false })

                  console.log(err)
                  this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


                });
            }}
          >

            सम्पादन गर्नुहोस्
          </Button>


        </Fragment >
    )
  };
}
const mapStateToProps = state => {
  console.log(state.book.categorys)
  return {
    editions: state.book.editions,
    token: state.auth.token,
    loading: state.book.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPostBookEdition: (editionName, token) => dispatch(postBookEdition(editionName, token)),
    onGetBookEdition: (token) => dispatch(getBookEdition(token)),
    ondeleteBookEdition: (editionId, token) => dispatch(deleteBookEdition(editionId, token)),
    editBookEdition: (newEdition, editionId, token) => dispatch(editBookEdition(newEdition, editionId, token)),
    authCheckState: () => dispatch(authCheckState()),

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component);
