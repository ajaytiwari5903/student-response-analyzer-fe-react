import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { PageTitle } from '../../../layout/Admin';
import { Router, browserHistory, IndexRoute } from 'react-router'
import { initialState } from '../../../store/reducers/book'
import { authCheckState } from '../../../store/actions/auth';
import { mainLink } from '../../../store/utils'
import Alert from '@material-ui/lab/Alert';
import Hoc from '../../../hoc/hoc'
import {
  Button,
  Checkbox,
  Icon,
  Divider,
  Paper,
  FormControlLabel
} from '@material-ui/core';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import hero9 from '../../../assets/images/hero-bg/hero-8.jpg';
import { SignUpForm } from '../../../components/FormFields/index';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition, getDetailBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import axios from 'axios'
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import { NepaliDatePicker } from "nepali-datepicker-reactjs";
import "nepali-datepicker-reactjs/dist/index.css"
import dateMap from "./date"
import "nepali-datepicker-reactjs/dist/index.css"

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100ch'
    },
  },
});


const getKeyByValue = (object, value) => {
  return Object.keys(object).find(key => object[key] === value);
}

const detaildetition = {}
class CategoryForm extends React.Component {
  constructor(props) {
    super(props);
    let id = this.props.match.params.id
    console.log(id)
    if (id !== undefined && id !== null) {
      if (this.props.token !== undefined && this.props.token !== null) {
        this.props.ongetDetailBookEdition(this.props.token, id)
      }
    }
  }
  state = {
    name: '',
    end_date: '',
    start_date: '',
    total_number_of_days: '',
    total_number_of_session: '',
    teptitle: '',
    redirect: null,
    error: false,
    saha_sayojug: '',
    sayojug: '',
    prabidik_karmachari: '',
    karyalaya_sahayogi: '',
    logimage: null,
    logimageUrl: '',
    name_english: '',
    certificate_issue_date: '',
    start_date_np: '',
    end_date_np: '',
    certificate_issue_date_np: ''
  }
  componentDidMount() {
    let d = new Date()
    let day = d.getMonth();
    let month = d.getMonth();
    if (day.toString().length == 1) {
      day = '0' + d.getDay()
    } else {
      day = d.getDay()
    }
    if (month.toString().length == 1) {
      month = '0' + d.getMonth()
    } else {
      month = d.getMonth()
    }
    console.log(dateMap[`${d.getFullYear()}-${month}-${day}`])
    this.setState({ dateBS: dateMap[`${d.getFullYear()}-${month}-${day}`] })
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    if (this.props.match.params.id != null && this.props.match.params.id != undefined) {
      this.setState({ loading: true })
      axios
        .get(window.location.origin + `/api/v1/traninigs/${this.props.match.params.id}/`)
        .then(res => {
          this.setState({ loading: false })
          let data = res.data
          console.log(data)
          this.setState({
            name: data.data.name,
            end_date: data.data.end_date,
            start_date: data.data.start_date,
            total_number_of_days: data.data.total_number_of_days,
            total_number_of_session: data.data.total_number_of_session,
            saha_sayojug: data.comitteeDate.saha_sayojug,
            sayojug: data.comitteeDate.sayojug,
            prabidik_karmachari: data.comitteeDate.prabidik_karmachari,
            karyalaya_sahayogi: data.comitteeDate.karyalaya_sahayogi,
            logimage: data.comitteeDate.sayojug_signature,
            logimageUrl: data.comitteeDate.sayojug_signature,
            name_english: data.data.name_english,
            certificate_issue_date: data.data.certificate_issue_date,
            certificate_issue_date_np: data.data.certificate_issue_date in dateMap ? dateMap[data.data.certificate_issue_date] : data.data.certificate_issue_date,
            start_date_np: data.data.start_date in dateMap ? dateMap[data.data.start_date] : data.data.start_date,
            end_date_np: data.data.end_date in dateMap ? dateMap[data.data.end_date] : data.data.end_date,
          })
          console.log(this.state)
          console.log(data.start_date in dateMap ? dateMap[data.data.start_date] : data.data.start_date)
          console.log(data.end_date in dateMap ? dateMap[data.data.end_date] : data.data.end_date)

        }
        )
        .catch(err => {
          this.setState({ loading: false })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


        });
    }
  }
  render() {

    const handelSubmit = (e) => {
      e.preventDefault();
      let data = new FormData();
      data.append('dateBS', this.state.dateBS)
      data.append('name', this.state.name);
      data.append('end_date', this.state.end_date);
      data.append('start_date', this.state.start_date);
      data.append('total_number_of_days', this.state.total_number_of_days);
      data.append('total_number_of_session', this.state.total_number_of_session);
      data.append('saha_sayojug', this.state.saha_sayojug);
      data.append('sayojug', this.state.sayojug);
      data.append('prabidik_karmachari', this.state.prabidik_karmachari);
      data.append('karyalaya_sahayogi', this.state.karyalaya_sahayogi);
      data.append('sayojug_signature', this.state.logimage);
      data.append('name_english', this.state.name_english);
      data.append('certificate_issue_date', this.state.certificate_issue_date);

      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/traninigs/", data)
        .then(res => {
          data = res.data
          console.log(data)
          if (data.existence) {
            this.setState({ isErrorDialogPopUp: true, ErrorMessage: data.existence })
          }
          else if (data.required) {
            document.getElementById(data.required).focus();
          }
          else if (data.success) {
            this.setState({ redirect: '/admin/traning' });
          }
        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
        });


      // this.setState({ redirect: '/admin/traning' });


    }
    const getKeyByValue = (object, value) => {
      return Object.keys(object).find(key => object[key] === value);
    }
    const handelSubmitAgain = (e) => {
      console.log(this.state)
      e.preventDefault();
      let data = new FormData();
      data.append('dateBS', this.state.dateBS)
      data.append('name', this.state.name);
      data.append('end_date', this.state.end_date);
      data.append('start_date', this.state.start_date);
      data.append('total_number_of_days', this.state.total_number_of_days);
      data.append('total_number_of_session', this.state.total_number_of_session);
      data.append('saha_sayojug', this.state.saha_sayojug);
      data.append('sayojug', this.state.sayojug);
      data.append('prabidik_karmachari', this.state.prabidik_karmachari);
      data.append('karyalaya_sahayogi', this.state.karyalaya_sahayogi);
      data.append('sayojug_signature', this.state.logimage);
      data.append('name_english', this.state.name_english);
      data.append('certificate_issue_date', this.state.certificate_issue_date);
      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/traninigs/", data)
        .then(res => {
          data = res.data
          console.log(data)
          if (data.required) {
            document.getElementById(data.required).focus();
          }
          else if (data.success) {
            this.setState({
              name: '',
              total_number_of_days: '',
              total_number_of_session: '',
              start_date: '',
              end_date: '',
              saha_sayojug: '',
              sayojug: '',

              prabidik_karmachari: '',
              karyalaya_sahayogi: '',
              logimage: null,
              logimageUrl: '',
              name_english: '',
              certificate_issue_date: '',
              start_date_np: '',
              end_date_np: '',
              certificate_issue_date_np: ''
            })

          }
        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

        });
    }
    const handelUpdate = () => {

      let data = new FormData();
      data.append('name', this.state.name);
      data.append('dateBS', this.state.dateBS)
      data.append('end_date', this.state.end_date);
      data.append('start_date', this.state.start_date);
      data.append('total_number_of_days', this.state.total_number_of_days);
      data.append('total_number_of_session', this.state.total_number_of_session);
      data.append('saha_sayojug', this.state.saha_sayojug);
      data.append('sayojug', this.state.sayojug);
      data.append('prabidik_karmachari', this.state.prabidik_karmachari);
      data.append('karyalaya_sahayogi', this.state.karyalaya_sahayogi);
      data.append('sayojug_signature', this.state.logimage);
      data.append('name_english', this.state.name_english);
      data.append('certificate_issue_date', this.state.certificate_issue_date);
      axios
        .put(window.location.origin + `/api/v1/traninigs/${this.props.match.params.id}/`, data)
        .then(res => {
          console.log(res.data)
          let data = res.data
          if (data.existence && data.required) {
            console.log(data.existence)
            this.setState({ isErrorDialogPopUp: true, ErrorMessage: data.existence })
            this.setState({ required: data.required })
          }
          else if (data.required) {
            document.getElementById(data.required).focus();
          }
          else if (data.success) {
            this.setState({ redirect: `/admin/traning/` });
          }
        }).catch(err => {
          console.log(err)
        })

    }

    const { classes } = this.props;
    const { error } = this.props;
    var { teptitle } = this.state
    console.log(this.state.detatilEdition, this.state.teptitle)
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    const loghandleImageChange = (e) => {
      if (e.target.files[0]) {
        // console.log(e.target.files[0].name.split('.png').length == 2)
        var isValid = /\.png?g$/i.test(e.target.files[0].name);
        if (!isValid) {
          alert('Only png files allowed!');
        } else {
          this.setState({
            logimage: e.target.files[0],
            logimageUrl: URL.createObjectURL(e.target.files[0]),
          })
        }
      }
    }
    return (
      <Hoc>
        <Dialog
          open={this.state.isErrorDialogPopUp}
          onClose={handleErrorClose}
          TransitionComponent={Transition}
          // PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Error Message
              </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleErrorClose} color="primary" autoFocus>
              Ok
                </Button>
          </DialogActions>
        </Dialog>
        <Card variant="outlined" >
          <div style={{ float: 'right', padding: 15 }}>
            <Link to="/admin/traning" ><ArrowBackIcon style={{}} /> </Link>
          </div>
          <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>
            {this.props.match.params.id != null && this.props.match.params.id != undefined ? "प्रशिक्षण सम्पादन गर्नुहोस्" : 'प्रशिक्षण थप्नुहोस्'}

          </div>

          <Divider className="my-1" />
          <CardContent style={{ padding: 5 }}>
            <CardContent className={classes.root} noValidate autoComplete="off">
              <FormControlLabel
                control={
                  <TextField id="name"
                    autoFocus
                    defaultValue={this.state.name}
                    label="
                    नाम" type='text' name='name'
                    value={this.state.name}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ name: e.target.value }) }} />
                }
              />
              <FormControlLabel
                control={
                  <TextField id="name_english"
                    defaultValue={this.state.name_english}
                    label="
                    Name" type='text' name='name_english'
                    value={this.state.name_english}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ name_english: e.target.value }) }} />
                }
              />
              <div>
                <div className="MuiFormControl-root MuiTextField-root" style={{ width: "50%" }}>
                  <label
                    id='certificate_issue_date'

                    className="MuiFormLabel-root MuiInputLabel-root MuiInputLabel-formControl MuiInputLabel-focused  MuiInputLabel-shrink"
                    data-shrink="true" htmlFor="end_date" >
                    प्रमाणपत्र जारी मिति</label>
                  <NepaliDatePicker
                    className="MuiInputBase-root MuiInput-root MuiInput-underline MuiInputBase-formControl MuiInput-formControl MuiInputLabel-focused"
                    inputClassName="MuiInputBase-input MuiInput-input"
                    id='certificate_issue_date_np'
                    value={this.state.certificate_issue_date_np != '' ? this.state.certificate_issue_date_np : null}
                    onChange={(value) => this.setState({ certificate_issue_date_np: value, certificate_issue_date: getKeyByValue(dateMap, value) })}
                    options={{ calenderLocale: "ne", valueLocale: "en" }}
                  />
                </div>
              </div>

              <div>

                <div className="MuiFormControl-root MuiTextField-root" style={{ width: "24%" }}>
                  <label
                    className="MuiFormLabel-root MuiInputLabel-root MuiInputLabel-formControl MuiInputLabel-focused  MuiInputLabel-shrink"
                    data-shrink="true" htmlFor="end_date" id="start_date">
                    सुरू मिति</label>
                  <NepaliDatePicker
                    id='start_date'
                    className="MuiInputBase-root MuiInput-root MuiInput-underline MuiInputBase-formControl MuiInput-formControl MuiInputLabel-focused"
                    inputClassName="MuiInputBase-input MuiInput-input"
                    value={this.state.start_date_np != '' ? this.state.start_date_np : null}
                    onChange={(value) => this.setState({ start_date_np: value, start_date: getKeyByValue(dateMap, value) })}
                    options={{ calenderLocale: "ne", valueLocale: "en" }}
                  />
                </div>
                <div className="MuiFormControl-root MuiTextField-root" style={{ width: "24%", marginLeft: '2%' }}>
                  <label
                    id='end_date'
                    className="MuiFormLabel-root MuiInputLabel-root MuiInputLabel-formControl MuiInputLabel-focused MuiInputLabel-shrink "
                    data-shrink="true" htmlFor="end_date" id="end_date">
                    अन्त्य मिति</label>
                  <NepaliDatePicker
                    id='end_date'
                    className="MuiInputBase-root MuiInput-root MuiInput-underline MuiInputBase-formControl MuiInput-formControl MuiInput-formControl MuiInputLabel-focused"
                    inputClassName="MuiInputBase-input MuiInput-input"
                    value={this.state.end_date_np != '' ? this.state.end_date_np : null}
                    onChange={(value) => this.setState({ end_date_np: value, end_date: getKeyByValue(dateMap, value) })}
                    options={{ calenderLocale: "ne", valueLocale: "en" }}
                  />
                </div>
              </div>

              {/* <FormControlLabel
                control={
                  <TextField id="total_number_of_session"
                    defaultValue={this.state.total_number_of_session}
                    label="सत्र संख्या" type='text' name='total_number_of_session'
                    value={this.state.total_number_of_session}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ total_number_of_session: e.target.value }) }} />
                }
              /> */}
              <FormControlLabel
                control={
                  <TextField id="sayojug"
                    defaultValue={this.state.sayojug}
                    label="संयोजक" type='text' name='sayojug'
                    value={this.state.sayojug}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ sayojug: e.target.value }) }} />
                }
              />
              <div>

                सयोजग
                हस्ताक्षर
                <input accept="image/png" style={{ width: '200px', display: 'none' }} onChange={loghandleImageChange} className={classes.input} id="log-button-file" type="file" />
                <label htmlFor="log-button-file">
                  <IconButton color="primary" aria-label="upload picture" component="span">
                    {this.state.logimageUrl ?
                      <img style={{ height: 100, width: 100, float: 'right' }} src={this.state.logimageUrl} /> :
                      <PhotoCamera />

                    }
                  </IconButton>
                </label>
              </div>
              <FormControlLabel
                control={
                  <TextField id="saha_sayojug"
                    defaultValue={this.state.saha_sayojug}
                    label="सह संयोजक " type='text' name='saha_sayojug'
                    value={this.state.saha_sayojug}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ saha_sayojug: e.target.value }) }} />
                }
              />
              <FormControlLabel
                control={
                  <TextField id="prabidik_karmachari"
                    defaultValue={this.state.prabidik_karmachari}
                    label="प्राविधिक कर्मचारी" type='text' name='prabidik_karmachari'
                    value={this.state.prabidik_karmachari}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ prabidik_karmachari: e.target.value }) }} />
                }
              />
              <FormControlLabel
                control={
                  <TextField id="karyalaya_sahayogi"
                    defaultValue={this.state.karyalaya_sahayogi}
                    label="कार्यालय सहयोगी" type='text' name='karyalaya_sahayogi'
                    value={this.state.karyalaya_sahayogi}
                    style={{ width: "50%" }} onChange={(e) => { this.setState({ karyalaya_sahayogi: e.target.value }) }} />
                }
              />

              {this.props.match.params.id != null && this.props.match.params.id != undefined ?
                <div style={{ width: '500px' }}>

                  <Button
                    type='submit'
                    variant="outlined"
                    onClick={handelUpdate}
                  >
                    बुझाउनुहोस्</Button>
                </div>
                :
                <div style={{ width: '500px' }}>
                  <Hoc>
                    <Button
                      type='submit'
                      variant="outlined"
                      onClick={handelSubmit}
                    >
                      बुझाउनुहोस्</Button>
                    <Button variant="outlined"
                      onClick={handelSubmitAgain}
                      style={{ marginLeft: 10 }}>
                      बुझाउनुहोस् र जारी राख्नुहोस्</Button>
                  </Hoc>
                </div>

              }
            </CardContent>
          </CardContent>
        </Card>
      </Hoc >
    )
  }
}
const mapStateToProps = state => {
  console.log(state.book.detatilEdition)
  return {
    detatilEdition: state.book.detatilEdition,
    editions: state.book.editions,
    token: state.auth.token,
    loading: state.book.loading,
    error: state.book.error,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onPostBookEdition: (editionName, token) => dispatch(postBookEdition(editionName, token)),
    onGetBookEdition: (token) => dispatch(getBookEdition(token)),
    ondeleteBookEdition: (editionId, token) => dispatch(deleteBookEdition(editionId, token)),
    oneditBookEdition: (newEdition, editionId, token) => dispatch(editBookEdition(newEdition, editionId, token)),
    ongetDetailBookEdition: (token, id) => dispatch(getDetailBookEdition(token, id)),
    authCheckState: () => dispatch(authCheckState())
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(CategoryForm);
