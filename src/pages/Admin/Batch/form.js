import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { PageTitle } from '../../../layout/Admin';
import { Router, browserHistory, IndexRoute } from 'react-router'
import { initialState } from '../../../store/reducers/book'
import { authCheckState } from '../../../store/actions/auth';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { mainLink } from '../../../store/utils'
import Alert from '@material-ui/lab/Alert';
import Hoc from '../../../hoc/hoc'
import {
  Checkbox,
  Icon,
  Divider,
  Paper,
  FormControlLabel
} from '@material-ui/core';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import hero9 from '../../../assets/images/hero-bg/hero-8.jpg';
import { SignUpForm } from '../../../components/FormFields/index';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition, getDetailBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import axios from 'axios'
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Input, List } from 'antd';
import 'antd/dist/antd.css';
import { Upload, Button } from 'antd';
import MultiImageInput from 'react-multiple-image-input';
const { TextArea } = Input;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100ch'
    },
  },
});



const detaildetition = {}
class CategoryForm extends React.Component {
  constructor(props) {
    super(props);
    let id = this.props.match.params.id
    console.log(id)

  }
  state = {
    created_by: '',
    batch: '',
    description: '',
    data: [{ question: "" }]
  }


  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    if (this.props.match.params.id != null && this.props.match.params.id != undefined) {
      this.setState({ loading: true })
      axios
        .get(window.location.origin + `/api/v1/batch/${this.props.match.params.id}/`)
        .then(res => {
          this.setState({ loading: false })
          let data = res.data
          console.log(data)
          this.setState({
            batch: data['batch'],
            data: data['question']
          })
        }
        )
        .catch(err => {
          this.setState({ loading: false })

          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })


        });
    }
  }



  render() {

    const handelSubmit = (e) => {
      e.preventDefault();
      let temp = {
        'batch': this.state.batch,
        'qustions': this.state.data,


      }
      console.log(temp)

      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/batch/", temp)
        .then(res => {
          console.log(res)
          if (res.data.success) {
            this.setState({ redirect: '/admin/Batch' })
          }
        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
        });


      // this.setState({ redirect: '/admin/traning' });


    }
    const handelSubmitAgain = (e) => {
      console.log(this.state)
      e.preventDefault();
      let data = new FormData();
      let temp = {
        'batch': this.state.batch,
        'qustions': this.state.data,


      }
      axios.defaults.headers = {
        "Content-Type": "application/json",
        Authorization: `Token ${this.props.token}`,
      };
      axios
        .post(mainLink + "/api/v1/batch/", temp)
        .then(res => {
          data = res.data
          console.log(data)
          if (res.data.success) {
            this.setState({ batch: '', data: [{ question: "" }] })
          }

        }
        )
        .catch(err => {
          console.log(err)
          this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

        });

    }
    const handelUpdate = () => {

      let data = new FormData();
      // data.append('name', this.state.name);
      // data.append('userId', this.props.userId)
      // data.append('description', this.state.description)
      // data.append('images', this.state.images)
      let temp = {
        'batch': this.state.batch,
        'qustions': this.state.data,
      }
      axios
        .put(window.location.origin + `/api/v1/batch/${this.props.match.params.id}/`, temp)
        .then(res => {
          if (res.data.success) {
            this.setState({ redirect: '/admin/Batch' })
          }
        }).catch(err => {
          console.log(err)
        })


    }

    const { classes } = this.props;
    const { error } = this.props;
    var { teptitle } = this.state
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };


    // handle input change
    const handleInputChange = (e, index) => {
      const { name, value } = e.target;
      const list = this.state.data;
      list[index][name] = value;
      this.setState({ data: list })

    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
      const list = this.state.data;
      list.splice(index, 1);
      this.setState({ data: list })
    };

    // handle click event of the Add button
    const handleAddClick = () => {
      this.state.data.push({ question: "" })
      this.setState({ data: this.state.data })
    };
    return (
      <Hoc>
        <Dialog
          open={this.state.isErrorDialogPopUp}
          onClose={handleErrorClose}
          TransitionComponent={Transition}
          // PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Error Message
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleErrorClose} color="primary" autoFocus>
              Ok
            </Button>
          </DialogActions>
        </Dialog>
        <Card variant="outlined" >
          <div style={{ float: 'right', padding: 15 }}>
            <Link to="/admin/Batch" ><ArrowBackIcon style={{}} /> </Link>
          </div>
          <div className="font-size-lg font-weight-bold" style={{ padding: 10 }}>
            {this.props.match.params.id != null && this.props.match.params.id != undefined ? "Edit Batch" : 'Add Batch'}

          </div>

          <Divider className="my-1" />
          <CardContent style={{ padding: 5 }}>
            <CardContent className={classes.root} noValidate autoComplete="off">
              <div>
                <Input style={{ width: 250 }} value={this.state.batch} placeholder="Please Enter The Batch Name" onChange={(e) => this.setState({ batch: e.target.value })} />
              </div>
              <div style={{ marginTop: 40, textAlign: '-webkit-center' }}>
                {this.state.data.map((x, i) => {
                  return (
                    <div style={{ marginTop: 10, paddingRight: 20 }}>

                      <Input style={{ width: 450 }} name="question" value={x.question} placeholder="Please Enter The Question" onChange={e => handleInputChange(e, i)} />

                      {this.state.data.length - 1 === i &&
                        <Button
                          style={{ marginLeft: 20 }}
                          variant="outlined"
                          onClick={handleAddClick}>ADD</Button>

                      }
                      {this.state.data.length !== 1 &&

                        <Button
                          style={{ marginLeft: 20 }}

                          variant="outlined"
                          onClick={() => handleRemoveClick(i)}
                        >
                          Remove</Button>

                      }


                    </div>
                  );
                })}
                {/* <div style={{ marginTop: 20 }}>{JSON.stringify(this.state.data)}</div> */}
              </div>




              {this.props.match.params.id != null && this.props.match.params.id != undefined ?
                <div style={{ width: '500px' }}>

                  <Button
                    type='submit'
                    variant="outlined"
                    onClick={handelUpdate}
                  >
                    SUBMIT</Button>
                </div>
                :
                <div style={{ width: '500px', marginTop: 20 }}>
                  <Hoc>
                    <Button
                      type='submit'
                      variant="outlined"
                      onClick={handelSubmit}
                    >
                      SUBMIT</Button>
                    <Button variant="outlined"
                      onClick={handelSubmitAgain}
                      style={{ marginLeft: 10 }}>
                      SUBMIT AND ADD AGAIN</Button>
                  </Hoc>
                </div>

              }
            </CardContent>
          </CardContent>
        </Card>
      </Hoc >
    )
  }
}
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userId: state.auth.userId,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    authCheckState: () => dispatch(authCheckState())
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(CategoryForm);