import React, { Fragment } from "react";
import { WrapperSimple } from "../../../layout/Admin";
import MaterialTable from "material-table";
import { PageTitle } from "../../../layout/Admin";
import { authCheckState } from "../../../store/actions/auth";
import { mainLink } from "../../../store/utils";
import axios from "axios";
import Hoc from "../../../hoc/hoc";
import Loading from "../../../components/Loading/index";

import { MTableToolbar } from "material-table";
import { connect } from "react-redux";
import { Paper, Icon, Button } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import {
  Checkbox,
  Divider,
  FormControlLabel,
  TextField,
  MenuItem,
} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import {
  postBookEdition,
  getBookEdition,
  deleteBookEdition,
  editBookEdition,
} from "../../../store/actions/book";
import { compose } from "redux";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "20ch",
    },
  },
});

class Component extends React.Component {
  state = {
    data: [],
    formDisplay: false,
    loading: false,
  };
  componentDidMount() {
    this.props.authCheckState();
    this.getDataFromApi();
  }
  getDataFromApi = () => {
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true });
    axios
      .get(window.location.origin + "/api/v1/batch/")
      .then((res) => {
        this.setState({ loading: false });

        let data = res.data;
        console.log(data);
        this.setState({
          data: res.data,
        });
      })
      .catch((err) => {
        this.setState({ loading: false });

        // console.log(err)
        // this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })
      });
  };

  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true });
      console.log(this.state);
    };
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false });
    };
    return this.state.loading ? (
      <Fragment>
        <Loading />
      </Fragment>
    ) : (
      <Fragment>
        <Dialog
          open={this.state.isErrorDialogPopUp}
          onClose={handleErrorClose}
          TransitionComponent={Transition}
          // PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: "move" }} id="draggable-dialog-title">
            Error Message
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <a
                severity="warning"
                style={{ color: "red", fontSize: "small", marginLeft: 20 }}
              >
                {this.state.ErrorMessage}
              </a>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleErrorClose} color="primary" autoFocus>
              Ok
            </Button>
          </DialogActions>
        </Dialog>
        <MaterialTable
          title={<a style={{ fontSize: 20, fontWeight: "bold" }}>Batch</a>}
          columns={[
            {
              title: "Batch name",
              field: "batch",
              // render: rowData => <img src={rowData.image} style={{ height: 90 }} />
            },
          ]}
          data={this.state.data}
          options={{
            filtering: false,
            actionsColumnIndex: -1,
            emptyRowsWhenPaging: false,
            pageSize: 20,
            pageSizeOptions: [20, 50, 100],
          }}
          editable={{
            onRowDelete: (oldData) =>
              new Promise((resolve, reject) => {
                const dataDelete = this.state.data;
                const index = oldData.tableData.id;
                this.setState({ loading: true });
                axios
                  .delete(mainLink + `/api/v1/batch/${oldData.id}/`)
                  .then((res) => {
                    this.setState({ loading: false });
                    dataDelete.splice(index, 1);
                    this.setState([...dataDelete]);
                    this.getDataFromApi();
                  })
                  .catch((err) => {
                    this.setState({ loading: false });

                    console.log(err);
                    this.setState({
                      isErrorDialogPopUp: true,
                      ErrorMessage: err.message,
                    });
                  });
                console.log(index);
                resolve();
              }),
          }}
          actions={[
            {
              icon: "edit",
              tooltip: "Edit",
              onClick: (event, rowData) =>
                this.props.history.push(`/admin/Batch-edit/${rowData.id}`),
            },
          ]}
          components={{
            Toolbar: (props) => (
              <div>
                <div style={{ padding: 20, paddingBottom: 0 }}>
                  <Link
                    label="Chip 1"
                    color="secondary"
                    style={{ marginRight: 5, float: "right" }}
                    to="/admin/Batch-create/"
                  >
                    <AddIcon style={{ fontSize: 40 }} />
                  </Link>
                </div>
                <MTableToolbar {...props} />
              </div>
            ),
          }}
        />
      </Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    userId: state.auth.userId,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions:
      typeof state.auth.user["permissions"] != "undefined"
        ? state.auth.user.permissions
        : [],
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authCheckState: () => dispatch(authCheckState()),
  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(Component);
