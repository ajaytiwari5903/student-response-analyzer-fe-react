import React, { Fragment } from 'react';
import { connect } from "react-redux";
import Alert from '@material-ui/lab/Alert';
import { authCheckState } from '../../../store/actions/auth';
import CardContent from '@material-ui/core/CardContent';
import { compose } from 'redux';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Button,
  Checkbox,
  TextField,
  FormControlLabel,
  InputLabel
} from '@material-ui/core';
import { WrapperSimple } from '../../../layout/Admin';
import { getSetting, postSetting } from "../../../store/actions/setting";
import { SelectPicker } from "rsuite";
import Select from "@material-ui/core/Select";
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '100ch'
    },
    input: {
      display: 'none',
    },
  },
})






class Setting extends React.Component {
  state = {
    print_on_submit: true,
    name: '',
    address: '',
    phone_number: '',
    print_size: '3.5',
    data: {},
    errorMessage: '',
    sigimage: null,
    sigimageUrl: '',
    logimage: null,
    logimageUrl: '',
    headmaster_name: '',
    video_link: 'https://www.youtube.com/watch?v=eKqWrEL-wgk',
    google_key:'AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw',
  }

  componentDidMount() {
    this.props.getSetting(this.props.token)
    this.props.authCheckState()
    console.log(this.props.setting)
    this.setState({
      name: this.props.setting.name,
      address: this.props.setting.address,
      headmaster_name: this.props.setting.headmaster_name,
      phone_number: this.props.setting.phone_number,
      sigimage: this.props.setting.principal_signature,
      sigimageUrl: this.props.setting.principal_signature,
      logimage: this.props.setting.logo,
      logimageUrl: this.props.setting.logo,
      mail_email: this.props.setting.mail_email,
      mail_subject: this.props.setting.mail_subject,
      mail_text: this.props.setting.mail_text,
      mail_password: this.props.setting.mail_password,
      video_link: this.props.setting.video_link,
      google_key: this.props.setting.google_key,


    })
  }


  static getDerivedStateFromProps(nextProps, prevState,/* nextProps2,prevState2 */) {
    if (nextProps.settings && nextProps.setting !== prevState.data) {
      return { data: nextProps.settings, print_on_submit: nextProps.setting.print_on_submit, name: nextProps.setting.name }
    }
    else return null;
  }

  render() {
    const { classes } = this.props;
    const handelSubmit = (e) => {
      e.preventDefault();
      if (!this.state.name) {
        this.setState({ errorMessage: 'Library Name Field is required' })
      } else {
        this.setState({ errorMessage: '' })
        this.props.postSetting(this.state, this.props.token,
          () => {
            //success
            alert("Setting Updated")
          }, () => {
            //error
            this.setState({
              errorMessage: 'Something went wrong'
            })
          })

      }
    }
    const sighandleImageChange = (e) => {
      if (e.target.files[0]) {
        this.setState({
          sigimage: e.target.files[0],
          sigimageUrl: URL.createObjectURL(e.target.files[0]),
        })
      }
    }
    const loghandleImageChange = (e) => {
      if (e.target.files[0]) {
        this.setState({
          logimage: e.target.files[0],
          logimageUrl: URL.createObjectURL(e.target.files[0]),
        })
      }
    }
    return (
      < Fragment >
        <WrapperSimple sectionHeading="SETTING">
          {
            this.state.errorMessage
            &&
            <Alert severity="warning" style={{ padding: '5px', }}>{this.state.errorMessage}</Alert>
          }
          <CardContent className={classes.root}>
            <div>
              <FormControlLabel
                style={{ width: '40%' }}
                control={
                  <TextField
                    type="text"
                    value={this.state.name}
                    style={{ width: "100%" }}
                    onChange={(e) => { this.setState({ name: e.target.value }) }}
                    label='
                    INSTITUTION NAME'
                  >
                  </TextField>
                }
              />
            </div>
            <div>
              <FormControlLabel
                style={{ width: '40%' }}

                control={
                  <TextField
                    type="text"
                    value={this.state.address}
                    style={{ width: "100%" }}
                    onChange={(e) => { this.setState({ address: e.target.value }) }}
                    label='ADDRESS'
                  >
                  </TextField>
                }
              />
            </div>
            <div>
              <FormControlLabel
                style={{ width: '40%' }}

                control={
                  <TextField
                    type="text"
                    value={this.state.phone_number}
                    style={{ width: "100%" }}
                    onChange={(e) => {
                      this.setState({ phone_number: e.target.value.match(/(-|[0-9])/g) ? e.target.value.match(/(-|[0-9])/g).join('') : '' })
                    }}
                    label='PHONE NUMBER'
                  >
                  </TextField>
                }
              />
            </div>
            <div>
              <FormControlLabel
                  style={{ width: '40%' }}

                  control={
                    <TextField
                        type="text"
                        value={this.state.video_link}
                        style={{ width: "100%" }}
                        onChange={(e) => { this.setState({ video_link: e.target.value }) }}
                        label='YOUTUBE CHANNEL LINK'
                    >
                    </TextField>
                  }
              />
            </div>
            <div>
              <FormControlLabel
                  style={{ width: '40%' }}

                  control={
                    <TextField
                        type="text"
                        value={this.state.google_key}
                        style={{ width: "100%" }}
                        onChange={(e) => { this.setState({ google_key: e.target.value }) }}
                        label='GOOGLE KEY'
                    >
                    </TextField>
                  }
              />
            </div>
            <div>
              <FormControlLabel
                style={{ width: '40%' }}

                control={
                  <TextField
                    type="text"
                    value={this.state.mail_email}
                    style={{ width: "100%" }}
                    onChange={(e) => { this.setState({ mail_email: e.target.value }) }}
                    label='EMAIL'
                  >
                  </TextField>
                }
              />
            </div>


            {/* <div>
              <FormControlLabel
                style={{ width: '40%' }}

                control={
                  <TextField
                    type="password"
                    value={this.state.mail_password}
                    style={{ width: "100%" }}
                    onChange={(e) => { this.setState({ mail_password: e.target.value }) }}
                    label='Password'
                  >
                  </TextField>
                }
              />
            </div> */}
            <div>
              <Button
                type='submit'
                variant="outlined"
                onClick={handelSubmit}
              >Submit</Button>
            </div>
          </CardContent>
        </WrapperSimple>
      </Fragment >
    )
  }

}



const mapStateToProps = state => {
  let settings = state.setting.settings
  console.log(settings)
  return {
    error: state.setting.error,
    token: state.auth.token,
    setting: state.setting.setting,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getSetting: (token) => dispatch(getSetting(token)),
    postSetting: (data, token, onSuccess, onError) => dispatch(postSetting(data, token, onSuccess, onError)),
    authCheckState: () => dispatch(authCheckState())
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Setting);
