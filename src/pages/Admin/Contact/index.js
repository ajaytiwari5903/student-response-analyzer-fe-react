import React, { Fragment } from 'react';
import { WrapperSimple } from '../../../layout/Admin';
import MaterialTable from 'material-table';
import { PageTitle } from '../../../layout/Admin';
import { authCheckState } from '../../../store/actions/auth';
import { mainLink } from '../../../store/utils'
import axios from 'axios'
import Hoc from '../../../hoc/hoc';
import Loading from '../../../components/Loading/index';

import { MTableToolbar } from 'material-table'
import { connect } from "react-redux";
import { Paper, Icon, Button } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import {
  Checkbox,
  Divider,
  FormControlLabel,
  TextField,
  MenuItem
} from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { postBookEdition, getBookEdition, deleteBookEdition, editBookEdition } from '../../../store/actions/book'
import { compose } from 'redux';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});





class Component extends React.Component {
  state = {
    data: [],
    formDisplay: false,
    loading: false,
  }
  componentDidMount() {
    this.props.authCheckState()
    axios.defaults.headers = {
      "Content-Type": "application/json",
      Authorization: `Token ${this.props.token}`,
    };
    this.setState({ loading: true })
    axios
      .get(window.location.origin + "/api/v1/PublicContactRequest/")
      .then(res => {
        this.setState({ loading: false })

        let data = res.data
        console.log(data)
        this.setState({
          data: res.data
        })
      }
      )
      .catch(err => {
        this.setState({ loading: false })

        // console.log(err)
        // this.setState({ isErrorDialogPopUp: true, ErrorMessage: err.message })

      });

  }

  render() {
    const handelAddClick = (e) => {
      e.preventDefault();
      this.setState({ formDisplay: true })
      console.log(this.state)
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };
    return (
      this.state.loading ?
        <Fragment>
          <Loading />
        </Fragment>

        :
        <Fragment>
          <Dialog
            open={this.state.isErrorDialogPopUp}
            onClose={handleErrorClose}
            TransitionComponent={Transition}
            // PaperComponent={PaperComponent}
            aria-labelledby="draggable-dialog-title"
          >
            <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
              Error Message
              </DialogTitle>
            <DialogContent>
              <DialogContentText>
                <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleErrorClose} color="primary" autoFocus>
                Ok
                </Button>
            </DialogActions>
          </Dialog>
          <MaterialTable
            title={<a style={{ fontSize: 20, fontWeight: 'bold' }}>
              CONTACT</a>}
            columns={[
              {
                title: 'NAME',
                emptyValue: "",
                field: 'name',


              },
              {
                title: 'SUBJECT',
                field: 'subject',
                emptyValue: "",
                // render: rowData => <a>{rowData.created_by.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.created_by}</a>,

              },

              {
                title: 'EMAIL',
                field: 'email',
                emptyValue: "",
                // render: rowData => <a>{rowData.created_by.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.created_by}</a>,

              },
              {
                title: 'MESSAGE',
                field: 'message',
                emptyValue: "",
                // render: rowData => <a>{rowData.created_by.length >= 25 ? `${rowData.name.substr(0, 25)}...` : rowData.created_by}</a>,

              },
            ]}
            data={this.state.data}
            options={
              {
                filtering: false,
                actionsColumnIndex: -1,
                emptyRowsWhenPaging: false,
                pageSize: 20,
                pageSizeOptions: [20, 50, 100]
              }
            }



          />


        </Fragment >
    )
  };
}
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    userId: state.auth.userId,
    isAuthenticated: state.auth.token !== null,
    successAuth: state.auth.successAuth,
    permissions: (typeof state.auth.user['permissions'] != 'undefined') ? state.auth.user.permissions : []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    authCheckState: () => dispatch(authCheckState()),

  };
};
export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(Component);