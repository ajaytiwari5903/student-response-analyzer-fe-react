import React, { Fragment } from 'react';
import {
  Button,
  Checkbox,
  Divider,
  FormControlLabel
} from '@material-ui/core';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import hero10 from '../../../assets/images/hero-bg/hero-10.jpeg';
import { SignUpForm } from '../../../components/FormFields/index';
import { authSignUp } from '../../../store/actions/auth';
import { compose } from 'redux';
const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});

export const validate = (values, requiredfields) => {
  const errors = {}
  const requiredFields = requiredfields
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}




class SignUpPage extends React.Component {
  state = {
    name: '',
    password: '',
    showPassword: false
  }

  render() {
    const handelSubmit = (e) => {
      e.preventDefault();
      console.log(this.state)
    }

    const { classes } = this.props;
    return (
      <Fragment>
        <div className="hero-wrapper bg-composed-wrapper bg-premium-dark min-vh-100">
          <div className="flex-grow-1 w-100 d-flex align-items-center">
            <div
              className="bg-composed-wrapper--image opacity-9"
              style={{ backgroundImage: 'url(' + hero10 + ')' }}
            />
            <div className="bg-composed-wrapper--bg bg-second opacity-3" />
            <div className="bg-composed-wrapper--bg bg-red-lights opacity-1" />
            <div className="bg-composed-wrapper--content pt-5 pb-2 py-lg-5" style={{ textAlign: '-webkit-center' }}>
              < SignUpForm {...this.props} />
            </div>
          </div>
        </div>
        {/* <Button
          to="/admin/DashboardDefault"
          component={Link}
          size="large"
          color="primary"
          variant="contained"
          className="m-2 py-3 px-5"
          title="View Carolina React Admin Dashboard with Material-UI Free Live Preview">
          <span className="btn-wrapper--label">Go To Dashboard</span>
          <span className="btn-wrapper--icon">
            <FontAwesomeIcon icon={['fas', 'fa-sign-in ']} />
          </span>
        </Button> */}
      </Fragment >
    )
  };
};
const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuthSignUp: (name, address, email, password1, password2, pnumber, position, gender) =>
      dispatch(authSignUp(name, address, email, password1, password2, pnumber, position, gender))
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(SignUpPage);
