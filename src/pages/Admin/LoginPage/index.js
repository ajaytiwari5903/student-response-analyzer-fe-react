import React, { Fragment } from 'react';
import {
  Button,
  Checkbox,
  Divider,
  FormControlLabel
} from '@material-ui/core';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';

import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  withStyles,
} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import hero13 from '../../../assets/images/hero-bg/hero-14.svg';
import svg9 from '../../../assets/images/stock-logos/9.svg';
import png10 from '../../../assets/images/stock-logos/10.png';

import hero12 from '../../../assets/images/hero-bg/hero-12.png';
import { authLogin } from '../../../store/actions/auth';
import { compose } from 'redux';
import Slide from '@material-ui/core/Slide';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = (theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '20ch'
    },
  },
});

export const validate = (values, requiredfields) => {
  const errors = {}
  const requiredFields = requiredfields
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Required'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Invalid email address'
  }
  return errors
}




class LogInPage extends React.Component {
  state = {
    username: '',
    password: '',
    redirect: null,
    showPassword: false,
    hasErrorMessage: false,
    isErrorDialogPopUp: false,
    ErrorMessage: ''
  }
  componentDidMount() {
    const script = document.createElement("script");

    script.src = "./script";
    script.async = true;

    document.body.appendChild(script);
  }
  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    console.log("willmount")
    if (!this.state.hasErrorMessage && nextProps.error === null) {
      this.state.hasErrorMessage = false
    } else if (nextProps.error != null && this.props.error != nextProps.error) {
      console.log(nextProps.error)
      this.state.hasErrorMessage = true
      if ('message' in nextProps.error && nextProps.error.message.split(' ').includes('400')) {
        console.log(nextProps.error.message)
        this.state.isErrorDialogPopUp = true
        this.state.ErrorMessage = `Credentials Could Not be Verified`
      }
      else {
        this.state.ErrorMessage = nextProps.error.message
      }
    }
    else {
      this.state.hasErrorMessage = false
    }

  }
  render() {
    const handelSubmit = (e) => {
      e.preventDefault();
      const { username, password } = this.state
      this.props.onAuthLogin(username, password)
      setTimeout(() => {
        if (!this.state.hasErrorMessage) {
          // this.state.redirect = "/admin/DashboardDefault"
          this.props.history.push("/admin/DashboardDefault");
        }
      }, 1000);

    }
    // if (this.props.successAuth) {
    //   this.props.history.push("/admin/DashboardDefault");
    // }
    const { classes } = this.props;
    const { error } = this.props;
    const { successAuth } = this.props;
    console.log(this.props.classes)
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }
    const handleErrorClose = () => {
      this.setState({ isErrorDialogPopUp: false })
    };




    return (
      <Fragment>
        <Dialog
          open={this.state.isErrorDialogPopUp}
          onClose={handleErrorClose}
          TransitionComponent={Transition}
          // PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Error Message
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <a severity="warning" style={{ color: "red", fontSize: "small", marginLeft: 20 }}>{this.state.ErrorMessage}</a>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleErrorClose} color="primary" autoFocus>
              Ok
            </Button>
          </DialogActions>
        </Dialog>
        <div style={{display:'flex',justifyContent:'flex-end',marginTop:'30px',marginRight:'150px'}}>
          {/* <Link to="/trainee/register" aria-controls="simple-menu">
            प्रशिक्षार्थि दर्ता गर्नुहोस्
          </Link> */}
        </div>


        <img className="wave" src={hero12} />
        <div className="container">
          <div className="img">
            <img src={hero13} />

          </div>
          <div className="login-content" style={{ alignText: 'center' }}>
            <form onSubmit={handelSubmit}>
              <img src={svg9} />

              <h2 className="title">Welcome</h2>


              <div className="div">

                <FormControlLabel
                  className='loginUsername'
                  control={
                    <TextField style={{ width: 338 }} id="username" label={<h5>USERNAME</h5>} type='text' name='username' className='loginUsername' onChange={(e) => { this.setState({ username: e.target.value }) }} autoFocus />
                  }
                  lable={<h5>USERNAME</h5>}
                />
              </div>



              <div className="div">

                <FormControlLabel
                  control={
                    this.state.showPassword ?
                      <TextField id="password" style={{ width: 338 }} label={<h5>PASSWORD</h5>} type='text' name='password' onChange={(e) => { this.setState({ password: e.target.value }) }} />
                      :
                      <TextField id="password" style={{ width: 338 }} label={<h5>PASSWORD</h5>} type='password' name='password' onChange={(e) => { this.setState({ password: e.target.value }) }} />
                  }
                  lable='Password'
                />
                <br />
                <FormControlLabel
                  style={{}}
                  control={
                    <Checkbox
                      onChange={(e) => { this.setState({ showPassword: !this.state.showPassword }) }}
                      value="checked"
                    />
                  }
                  label="Show Password"
                />
              </div>

              <Button className='btn' type="submit" >LOGIN</Button>
            </form>
          </div>
        </div>

      </Fragment >
    )
  };
};
const mapStateToProps = state => {
  return {
    token: state.auth.token,
    loading: state.auth.loading,
    error: state.auth.error,
    successAuth: state.auth.successAuth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAuthLogin: (username, password) =>
      dispatch(authLogin(username, password))
  };
};

export default compose(
  withStyles(useStyles, { withTheme: true }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(LogInPage);
