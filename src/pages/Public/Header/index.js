import React from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";

import {
  Hidden,
  IconButton,
  AppBar,
  Box,
  Tooltip,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";

class Test extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <Fragment>
        <nav class="public-header-nav">
          <div class="row-container flexbox">
            <div class="box">
              <div class="left-icons">
                <a style={{ color: "white" }}>
                  <div class="box">
                    <Hidden mdDown>
                      <img
                        src="//static1.tvfplay.com/assets/web3.0/channel.svg"
                        alt="Channels"
                        class="show-desktop"
                      />
                    </Hidden>
                    <Hidden lgUp>
                      <img
                        src="//static1.tvfplay.com/assets/web3.0/mobile/channel.svg"
                        alt="Channels"
                        class="show-mobile"
                      />
                    </Hidden>
                    <Hidden mdDown>
                      <span class="show-desktop">SRA</span>
                    </Hidden>
                  </div>
                </a>

                <Hidden lgUp>
                  <div class="box show-mobile">
                    <img
                      src="//static1.tvfplay.com/assets/web3.0/mobile/search.svg"
                      alt="Search"
                    />
                  </div>
                </Hidden>
              </div>
            </div>
          </div>
        </nav>
      </Fragment>
    );
  }
}

export default Test;
