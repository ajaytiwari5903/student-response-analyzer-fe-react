import React from "react";
import { Fragment } from "react";
import Footer from "../Footer/index";
import SirImg from "../../../assets/images/PkkImage/testimg.jpg";
import hero12 from "../../../assets/images/hero-bg/hero-12.png";
import hero9 from "../../../assets/images/hero-bg/hero-9.jpg";
import hero8 from "../../../assets/images/hero-bg/hero-8.jpg";
import hero10 from "../../../assets/images/hero-bg/hero-10.jpeg";
import bg1 from "../../../assets/images/composed-bg/bg1.jpg";
import bg2 from "../../../assets/images/composed-bg/bg2.jpg";
import bg5 from "../../../assets/images/composed-bg/bg5.jpg";
import bg3 from "../../../assets/images/composed-bg/bg3.jpg";
import bg4 from "../../../assets/images/composed-bg/bg4.jpg";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Row, Col, Divider } from "antd";
import "antd/dist/antd.css";
import Tooltip from "@material-ui/core/Tooltip";
import { Carousel as SingleCarousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import axios from "axios";
import { Input, List, Button } from "antd";
import { Link } from "react-router-dom";

class PublicDashboard extends React.Component {
  state = {
    data: [{ batch: "batch1" }, { batch: "batch2" }],
  };

  componentDidMount() {
    axios
      .get(window.location.origin + "/api/v1/batch/")
      .then((res) => {
        console.log(res.data);
        this.setState({ data: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <Fragment>
        <div style={{ textAlign: "center", paddingBottom: 10 }}>
          {this.state.data.map((x, i) => {
            return (
              <div style={{ marginTop: 10, paddingRight: 20 }} key={i}>
                <Link to={`/public/Batch-public/${x.id}`}>
                  <Button type="dashed" style={{ width: 300 }} block>
                    {x.batch}
                  </Button>
                </Link>
              </div>
            );
          })}
        </div>
      </Fragment>
    );
  }
}
export default PublicDashboard;
