import React from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";

class PublicFooter extends React.Component {
  render() {
    return (
      <Fragment>
        <div id="public-footer" style={{}}>
          <div
            style={{
              paddingTop: "50px",
              paddingBottom: "30px",
              textAlign: "center",
              fontSize: "40px",
            }}
          >
            <span
              style={{ fontWeight: "bold", fontSize: "40px", color: "#ffc105" }}
            >
              Student Response Analyzes
            </span>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default PublicFooter;
