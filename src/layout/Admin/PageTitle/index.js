import React, { Fragment } from 'react';

import { Paper, Icon, Button } from '@material-ui/core';


function PageTitle(props) {

  return (
    <Fragment>
      <Paper square elevation={2} className="app-page-title">
        <div>
          <div className="app-page-title--first">
            <div className="app-page-title--heading">
              <h1>{props.titleHeading}</h1>
              <div className="app-page-title--description">
                {props.titleDescription}

              </div>
            </div>

          </div>
        </div>
        {/* <Button> <Icon style={{ fontSize: 50 }}>add_circle</Icon></Button> */}
      </Paper>
    </Fragment>
  );
}

export default PageTitle;
