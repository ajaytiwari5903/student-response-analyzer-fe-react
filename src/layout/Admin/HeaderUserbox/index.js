import React, { Fragment } from 'react';
import { logout } from '../../../store/actions/auth'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import {
  Avatar,
  Box,
  Menu,
  Button,
  List,
  ListItem,
  Tooltip,
  Divider
} from '@material-ui/core';
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    // fontSize: 11,
  },
}))(Tooltip);
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: '#3d4977',
      color: "white",
    },
  },
}))(MenuItem);

const HeaderUserbox = (props) => {
  const { logout } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handelLogout = (e) => {
    logout()
  }
  return (
    <Fragment>
      <LightTooltip title="LOGOUT" placement="top">

        <Button
          onClick={handelLogout}
          style={{ paddingRight: 0 }}
        >
          <ExitToAppIcon size='medium' style={{ color: '#0b76b8' }} />
        </Button>
      </LightTooltip>

      {/* <Button
        color="inherit"
        onClick={handleClick}
        className="text-capitalize px-3 text-left btn-inverse d-flex align-items-center">
        <Box>
          <Avatar sizes="44" alt="Admin" src='https://scontent.fktm8-1.fna.fbcdn.net/v/t1.0-1/p200x200/67149060_517212019034074_2706303144257650688_n.jpg?_nc_cat=109&_nc_sid=7206a8&_nc_oc=AQliYZzbIOzXWyP6xDiwVgtBpd3t2r2Lm1JjWK7yxA5AV--bi8cAA6G241aIjwpyuI8&_nc_ht=scontent.fktm8-1.fna&_nc_tp=6&oh=fdecade0dbd6fc8c57a13dd39c171b5f&oe=5F20DE58' />
        </Box>
        <div className="d-none d-xl-block pl-3">
          <div className="font-weight-bold pt-2 line-height-1">Admin</div>
          <span className="text-white-50">Developer</span>
        </div>
        <span className="pl-1 pl-xl-3">
          <FontAwesomeIcon icon={['fas', 'angle-down']} className="opacity-5" />
        </span>
      </Button> */}


      <StyledMenu
        anchorEl={anchorEl}
        keepMounted
        getContentAnchorEl={null}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className="ml-2">
        <div className="dropdown-menu-right dropdown-menu-lg overflow-hidden p-0" style={{ width: "320px" }}>
          <List className="text-left bg-transparent d-flex align-items-center flex-column pt-0">
            <Box display="flex" p="16px" justifyContent="center">
              <Box>
                <Avatar sizes="44" alt="Admin" src='https://scontent.fktm8-1.fna.fbcdn.net/v/t1.0-1/p200x200/67149060_517212019034074_2706303144257650688_n.jpg?_nc_cat=109&_nc_sid=7206a8&_nc_oc=AQliYZzbIOzXWyP6xDiwVgtBpd3t2r2Lm1JjWK7yxA5AV--bi8cAA6G241aIjwpyuI8&_nc_ht=scontent.fktm8-1.fna&_nc_tp=6&oh=fdecade0dbd6fc8c57a13dd39c171b5f&oe=5F20DE58' />

              </Box>
              <Box pl="20px">
                <div className="font-weight-bold text-left pt-2 line-height-1">
                  Admin
                </div>
                <span className="text-black-50 text-left">
                  Developer
              </span>
              </Box>
            </Box>
            <Divider className="w-100 mt-2" />
            <Link to="/">     <ListItem button style={{ color: 'red' }} onClick={(e) => handelLogout(e)}>
              Log Out
            </ListItem>
            </Link>
            <Divider className="w-100" />
            <ListItem className="d-block rounded-bottom px-3 pt-3 pb-0 text-center">
              <Tooltip arrow title="facebook">
                <a target="_blank" rel="noopener noreferrer" href='https://www.facebook.com/iamthakursaurav'>
                  <Button color="default" className="text-twitter">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'facebook']} />
                    </span>
                  </Button>
                </a>
              </Tooltip>
            </ListItem>
          </List>
        </div>
      </StyledMenu>
    </Fragment>
  );
}
const mapStateToProps = state => ({

  headerShadow: state.ThemeOptions.headerShadow,
  headerFixed: state.ThemeOptions.headerFixed,
  sidebarToggleMobile: state.ThemeOptions.sidebarToggleMobile
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(HeaderUserbox);