import React, { Fragment } from "react";

import clsx from "clsx";
import { Link, NavLink } from "react-router-dom";
import navItems from "../Sidebar/navItems";
import SidebarMenu from "../SidebarMenu";
import axios from "axios";
import {
  Hidden,
  IconButton,
  AppBar,
  Box,
  Tooltip,
  Button,
  Menu,
  MenuItem,
} from "@material-ui/core";

import { connect } from "react-redux";

import { setSidebarToggleMobile } from "../../../store/reducers/AdminTheme";
import projectLogo from "../../../assets/images/nepal-logo.png";

import HeaderLogo from "../HeaderLogo";
import HeaderUserbox from "../HeaderUserbox";
import MenuBar from "../MenuBar";
import MenuOpenRoundedIcon from "@material-ui/icons/MenuOpenRounded";
import MenuRoundedIcon from "@material-ui/icons/MenuRounded";
import Hoc from "hoc/hoc";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

const MenuContent = (
  <div>
    {navItems.map((list, index) => (
      <SidebarMenu
        key={index}
        component="div"
        pages={list.content}
        title={list.label}
      />
    ))}
  </div>
);

class Header extends React.Component {
  state = {
    height: 0,
  };
  componentDidMount() {
    if (
      this.props.match &&
      this.props.match != null &&
      this.props.match != undefined
    ) {
      axios
        .get(
          window.location.origin +
            `/api/v1/trainingwisesession/${this.props.match.params.id}/`
        )
        .then((res) => {
          this.setState({ loading: false });
          let data = res.data;
          console.log(data);
          this.setState({
            data: res.data.data,
            training: res.data.training,
            sayojug: res.data.sayojug,
            saha_sayojug: res.data.saha_sayojug,
            prabidik_karmachari: res.data.prabidik_karmachari,
            karyalaya_sahayogi: res.data.karyalaya_sahayogi,
          });
        })
        .catch((err) => {
          this.setState({ loading: false });

          console.log(err);
          this.setState({
            isErrorDialogPopUp: true,
            ErrorMessage: err.message,
          });
        });
    }
  }

  render() {
    const toggleSidebarMobile = () => {
      setSidebarToggleMobile(!sidebarToggleMobile);
    };
    const {
      headerShadow,
      headerFixed,
      sidebarToggleMobile,
      setSidebarToggleMobile,
    } = this.props;
    return (
      <Fragment>
        {/* <Hidden mdDown>
          <AppBar
            color='white'
            style={{ width: '100%' }}
            id="navbar"
            className={clsx('app-header', {})}
            position={headerFixed ? '' : ''}
            elevation={headerShadow ? 11 : 3}>
            {!this.props.isCollapsedLayout && <HeaderLogo {...this.props} />}
          </AppBar>

        </Hidden> */}
        <AppBar
          color="secondary"
          style={{
            backgroundImage: "linear-gradient(to right,#fff,#0b76b8,#fff)",
            height: 62,
          }}
          // style={{ background: '#18e1a5' }}
          className={clsx("app-header", {})}
          position={headerFixed ? "" : "absolute"}
          elevation={headerShadow ? 11 : 3}
        >
          <Box className="d-flex align-items-center">
            <Link to="/">
              <a
                style={{
                  color: "#0b76b8",
                  marginLeft: "20px",
                  fontSize: "20px",
                  fontWeight: "bold",
                }}
              >
                SRA
              </a>
            </Link>
          </Box>
          {/* {!props.isCollapsedLayout && <HeaderLogo />} */}
          <Box className="app-header-toolbar">
            <Hidden mdDown>
              <Box
                style={{
                  alignSelf: "center",
                }}
              >
                {/* < h1 > {name}</h1> */}
              </Box>
            </Hidden>
            <Hidden lgUp>
              <Box
                className="app-logo-wrapper"
                title="Carolina React Admin Dashboard with Material-UI Free"
              >
                <Link to="/DashboardDefault" className="app-logo-link">
                  <IconButton
                    color="primary"
                    size="medium"
                    className="app-logo-btn"
                  >
                    {/* <img
                      className="app-logo-img"
                      alt="Carolina React Admin Dashboard with Material-UI Free"
                      src={projectLogo}
                    /> */}
                  </IconButton>
                </Link>
                <Hidden smDown>
                  <Box className="app-logo-text">
                    {/* <div>{this.props.setting ? (this.props.setting.name.length <= 20 ? this.props.setting.name.substring(0, 10) : `${this.props.setting.name.substring(0, 10)} . . . `) : ''}</div> */}
                  </Box>
                </Hidden>
              </Box>
            </Hidden>
            <Hidden mdDown>
              <Box style={{ alignSelf: "center" }}>
                <Box className="d-flex align-items-center"></Box>

                {navItems.map((list, indexO) => (
                  <Box key={indexO} className="d-flex align-items-center">
                    <MenuBar permissions={this.props.permissions} />

                    {/* <Hoc >
                      {
                        this.props.permissions.length ?
                          !this.props.permissions || this.props.permissions.indexOf('setting-edit') >= 0 &&
                          <Link to={"/admin/search"}><Button style={{ color: 'white' }}>Search</Button></Link> :
                          <Link to={"/admin/search"}><Button style={{ color: 'white' }}>Search</Button></Link>
                      }

                    </Hoc> */}
                  </Box>
                ))}
              </Box>
            </Hidden>
            <Box className="d-flex align-items-center">
              <HeaderUserbox />
              <Box className="toggle-sidebar-btn-mobile">
                <Tooltip title="Toggle Sidebar" placement="right">
                  <IconButton
                    color="inherit"
                    onClick={toggleSidebarMobile}
                    size="medium"
                  >
                    {sidebarToggleMobile ? (
                      <MenuOpenRoundedIcon style={{ color: "#0b76b8" }} />
                    ) : (
                      <MenuRoundedIcon style={{ color: "#0b76b8" }} />
                    )}
                  </IconButton>
                </Tooltip>
              </Box>
            </Box>
          </Box>
        </AppBar>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  // setting: state.setting.setting,
  headerShadow: state.ThemeOptions.headerShadow,
  headerFixed: state.ThemeOptions.headerFixed,
  setting: state.setting.setting,
  sidebarToggleMobile: state.ThemeOptions.sidebarToggleMobile,
  permissions:
    typeof state.auth.user["permissions"] != "undefined"
      ? state.auth.user.permissions
      : [],
});

const mapDispatchToProps = (dispatch) => ({
  setSidebarToggleMobile: (enable) => dispatch(setSidebarToggleMobile(enable)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
