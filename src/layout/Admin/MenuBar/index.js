import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';

import { Link } from 'react-router-dom';

import MenuItem from '@material-ui/core/MenuItem';
import {
    Divider,
} from '@material-ui/core';
import Fade from '@material-ui/core/Fade';
import Hoc from '../../../hoc/hoc'
import { NavLink } from 'react-router-dom';
const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <ul
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            // backgroundColor: '#3d4977',
            color: "white",
            // width: 100,
            textAlign: 'center'
        },
        '&:hover': {
            backgroundColor: '#0b76b8 !important',
            color: 'white',
            minWidth: "10vw",

        }
    },
}))(MenuItem);

export default function CustomizedMenus(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleButtonLeave = () => {
        console.log('leave')
    }

    const { permissions } = props
    console.log(permissions)
    return (
        <Hoc >
            <div class="nav-btn" style={{}}>
                {/* <li class="nav-links">
                    <ul>
                        <li class="nav-link"  >
                            <a style={{ color: 'white', cursor: 'pointer' }}>MENU<i class="fas fa-caret-down"></i></a>
                            <div class="dropdown">
                                <ul>
                                    <li class="dropdown-link">
                                        <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 1</a>
                                    </li>
                                    <li class="dropdown-link">
                                        <a role="button" tabindex="1" style={{ cursor: 'pointer' }}>LINK 2</a>
                                    </li>
                                    <li class="dropdown-link">
                                        <a role="button" tabindex="3" style={{ cursor: 'pointer' }}>LINK 3<i style={{ color: '#0b76b8' }} class="fas fa-caret-down"></i></a>
                                        <div class="dropdown second">
                                            <ul>
                                                <li class="dropdown-link">
                                                    <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 1</a>
                                                </li>
                                                <li class="dropdown-link">
                                                    <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 2</a>
                                                </li>
                                                <li class="dropdown-link">
                                                    <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 3</a>
                                                </li>
                                                <li class="dropdown-link">
                                                    <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>More<i style={{ color: '#0b76b8' }} class="fas fa-caret-down"></i></a>
                                                    <div class="dropdown second">
                                                        <ul>
                                                            <li class="dropdown-link">
                                                                <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 1</a>
                                                            </li>
                                                            <li class="dropdown-link">
                                                                <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 2</a>
                                                            </li>
                                                            <li class="dropdown-link">
                                                                <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 3</a>
                                                            </li>
                                                            <div class="arrow"></div>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <div class="arrow"></div>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="dropdown-link">
                                        <a role="button" tabindex="0" style={{ cursor: 'pointer' }}>LINK 4</a>
                                    </li>
                                    <div class="arrow"></div>
                                </ul>
                            </div>
                        </li> 
                        </ul>
                        </li> */}

                <li class="nav-links">
                    <ul>
                    <li class="nav-link"  >
                            <Link to='/admin/DashboardDefault'>

                                <a style={{ color: 'white', cursor: 'pointer', textDecoration: 'none' }}>
                                    DASHBOARD</a>
                            </Link>
                        </li>
                        <li class="nav-link"  >
                            <Link to='/admin/Batch'>

                                <a style={{ color: 'white', cursor: 'pointer', textDecoration: 'none' }}>
                                    BATCH</a>
                            </Link>
                        </li>
                        
                    </ul>
                </li>
            </div>
        </Hoc>
    );
}