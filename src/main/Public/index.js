import React from 'react';
import PresentationLayout from 'main/PresentationLayout';
import Footer from '../../pages/Public/Footer/index';
import Header from '../../pages/Public/Header/index';
import axios from 'axios';
class PublicIndex extends React.Component {
    state = {
        data: {},
    }
    async componentDidMount() {
        console.log('componentDidMount colling ...');

        // axios.defaults.headers = {
        //   "Content-Type": "application/json",
        //   Authorization: `Token ${token}`,
        // };
        axios
            .get(window.location.origin + "/api/v1/settingsData/")
            .then(res => {
                console.log(res.data)
                this.setState({ data: res.data })

            }
            ).catch(err => {
                console.log(err)
            })
    }
    render() {
        return (
            <PresentationLayout>

                <Header data={this.state.data} />
                <div style={{ paddingTop: '70px' }}>
                    {this.props.children}
                </div>
                <div style={{ backgroundColor: '#000', color: 'white' }}>
                    <Footer data={this.state.data} />
                </div>
            </PresentationLayout>
        )
    }
}
export default PublicIndex;