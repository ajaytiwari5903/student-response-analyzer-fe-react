import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { createStore, compose, applyMiddleware, combineReducers } from "redux";
import { Provider } from "react-redux";
import { persistStore } from 'redux-persist'
import { persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import storage from 'redux-persist/lib/storage'
import thunk from "redux-thunk";
import authReducer from "./store/reducers/auth";
import settingReducer from "./store/reducers/setting"
import adminThemeReducer from './store/reducers/AdminTheme';
const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['ThemeOptions', 'auth', 'setting']
}
const rootReducer = combineReducers({
    ThemeOptions: adminThemeReducer,
    auth: authReducer,
    setting: settingReducer,
});

const store = createStore(persistReducer(persistConfig, rootReducer), composeEnhances(applyMiddleware(thunk)));
const persistor = persistStore(store);

const app = (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
registerServiceWorker();
